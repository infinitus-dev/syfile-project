<?php

namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
    require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Inicio');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Inicio::index', ['as' => 'inicio']);
$routes->get('utilizador/autenticacao', 'Inicio::iniciarSessao', ['as' => 'utilizador/autenticacao']);
$routes->get('utilizador/autenticacao/terminar-sessao', 'Inicio::terminarSessao', ['as' => 'utilizador/autenticacao/terminar-sessao']);
$routes->post('utilizador/autenticacao/validar-credencias', 'Inicio::autenticarUtilizador');
$routes->get('utilizadores/novo-registo', 'Inicio::pageRegistoUtilizador', ['as' => 'utilizadores/novo-registo']);
$routes->get('utilizadores/listar-utilizadores', 'Inicio::pageListarUtilizadores', ['as' => 'utilizadores/listar-utilizadores']);
$routes->get('utilizadores/ver-perfil/(:any)', 'Inicio::modalVerPerfilUtilizador/$1');
$routes->post('utilizadores/novo-registo/inserir', 'Inicio::inserirUtilizador');
$routes->post('utilizadores/grupo-acesso/inserir', 'Inicio::registarGrupoAcesso');
$routes->get('armazenamento/detalhes/(:any)/(:any)', 'Inicio::pageFolderDeteail/$1/$2');
$routes->get('armazenamento/ficheiro/detalhes/(:any)/(:any)', 'Inicio::pageVisualizarFile/$1/$2');
$routes->post('armazenamento/pasta/criar-nova-pasta', 'Inicio::criarPasta');
$routes->post('armazenamento/upload/carregar-ficheiro', 'Inicio::uploadFicheiros');
$routes->get('armazenamento/ficheiro/download/(:any)/(:any)', 'Inicio::downloadFiles/$1/$2');
$routes->post('armazenamento/ficheiro/editar-nome-ficheiro', 'Inicio::editarFicheiro');
$routes->post('armazenamento/ficheiro/mover-copiar-ficheiro', 'Inicio::moverCopiarFicheiro');
$routes->post('armazenamento/ficheiro/eliminar-ficheiros', 'Inicio::deleteFilesFolders');
$routes->post('armazenamento/ficheiro/catalogar-novos-ficheiros', 'Inicio::catalogarFilesFolders');


$routes->get('documentacao/emissao-documento','Inicio::pageEmissaoDocumento');
$routes->get('documentacao/previsualizar-documento', 'Inicio::pagePrevDocumento');
$routes->get('documentacao/previsualizar-documento/(:any)', 'Inicio::pagePrevDocumento/$1');
/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need to it be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
    require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
