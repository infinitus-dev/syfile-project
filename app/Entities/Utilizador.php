<?php


namespace app\Entities;


use CodeIgniter\Entity;
use CodeIgniter\I18n\Time;

class Utilizador extends Entity
{
    public function setPassword(string $pass)
    {
        $this->attributes['password'] = password_hash($pass, PASSWORD_BCRYPT);

        return $this;
    }
}