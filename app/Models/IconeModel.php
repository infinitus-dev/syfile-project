<?php

namespace App\Models;

use CodeIgniter\Model;

class IconeModel extends Model
{
    protected $table = 'icones';
    protected $primaryKey = 'idicone';
    protected $allowedFields = ['idicone', 'class'];
    protected $returnType = 'App\Entities\Icone';
}
