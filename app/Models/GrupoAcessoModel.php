<?php


namespace app\Models;


use CodeIgniter\Model;

class GrupoAcessoModel extends Model
{
    protected $table = 'grupoacessos';
    protected $primaryKey = 'idgrupoAcesso';
    protected $allowedFields = [
        'idgrupoAcesso',
        'icon', 'descricao', 'color'
    ];
    protected $returnType = 'App\Entities\Grupoacesso';

    public function generatePK()
    {
        # Gerar chave primaria de novo grupo de acesso
        $query = $this->db->query("select generate_custom_pk(5) as newPK");
        return $query->getRow()->newPK;
    }
}
