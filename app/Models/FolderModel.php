<?php


namespace app\Models;


use CodeIgniter\Model;

class FolderModel extends Model
{
    protected $nameItem, $path;
    private $type, $update_data, $memoria;

    /**
     * FolderModel constructor.
     * @param $type
     * @param $update_data
     */
    public function __construct($nameItem, $path)
    {
        $this->nameItem = $nameItem;
        $this->path = $path;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getUpdateData()
    {
        $this->update_data = date('D d-m-Y H:i:s', filemtime($this->path));
        return $this->update_data;
    }

    public function getContent()
    {
        return scan($this->path);
    }




}