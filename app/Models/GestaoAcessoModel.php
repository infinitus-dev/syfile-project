<?php


namespace app\Models;


use CodeIgniter\Model;

class GestaoAcessoModel extends Model
{
    protected $table = 'gestaoacessos';
    protected $primaryKey = 'utilizador';
    protected $allowedFields = [
        'utilizador', 'grupoAcesso', 'visualizar', 'transferir', 'mover', 'copiar', 'eliminar', 'editar'
    ];
    //protected $returnType = 'App\Entities\Gestaoacesso';
}