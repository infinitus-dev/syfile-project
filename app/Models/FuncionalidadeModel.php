<?php


namespace app\Models;


use CodeIgniter\Model;

class FuncionalidadeModel extends Model
{
    protected $table = 'funcionalidades';
    protected $primaryKey = 'idfuncionalidade';
    protected $allowedFields = ['idfuncionalidade',
        'descricao', 'modulo', 'link', 'icon', 'estado'
    ];
    protected $returnType = 'App\Entities\Funcionalidade';
}