<?php

namespace app\Models;

use CodeIgniter\Model;

class PerfilUtilizadorModel extends Model {
    protected $table = 'perfilutilizadores';
    protected $primaryKey = 'perfil';
    protected $allowedFields = ['perfil', 'nivelAcesso'];
    protected $returnType = 'App\Entities\PerfilUtilizador';
}