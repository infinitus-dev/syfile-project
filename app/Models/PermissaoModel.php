<?php


namespace app\Models;


use CodeIgniter\Model;

class PermissaoModel extends Model
{
    protected $table = 'permissoes';
    protected $primaryKey = 'descricao';
    protected $allowedFields = [
        'descricao'
    ];
    protected $returnType = 'App\Entities\Permissao';
}