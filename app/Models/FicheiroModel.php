<?php


namespace app\Models;


use CodeIgniter\Model;

class FicheiroModel extends Model
{
    protected $table         = 'ficheiros';
    protected $primaryKey = 'idficheiro';
    protected $allowedFields = ['idficheiro',
        'descricao', 'caminho', 'resumo', 'armazenamento', 'estado'
    ];
    protected $returnType    = 'App\Entities\Ficheiro';

    public function generate_key()
    {
        $sql = "select generate_custom_pk(4) newFile";
        $query = $this->db->query($sql);
        return $query->getFirstRow()->newFile;
    }
}