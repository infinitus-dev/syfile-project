<?php


namespace app\Models;


use CodeIgniter\Model;

class ArmazenamentoModel extends Model
{
    protected $table = 'armazenamentos';
    protected $primaryKey = 'idarmazenamento';
    protected $allowedFields = [
        'idarmazenamento',
        'origem', 'descricao', 'caminho', 'grupoAcesso', 'color', 'estado'
    ];
    protected $returnType = 'App\Entities\Armazenamento';

    public function generate_key()
    {
        $sql = "select generate_custom_pk(3) newFolder";
        $query = $this->db->query($sql);
        return $query->getFirstRow()->newFolder;
    }
}
