<?php


namespace app\Models;


use CodeIgniter\Model;

class UtilizadorModel extends Model
{
    protected $table = 'utilizadores';
    protected $primaryKey = 'idutilizador';
    protected $modelGestaoAcesso, $modelGrupoAcesso, $modelFolders;
    protected $allowedFields = [
        'idutilizador',
        'nome', 'password', 'email', 'created_at', 'updated_at', 'perfil'
    ];
    protected $returnType = 'App\Entities\Utilizador';
    protected $useTimestamps = true;

    public function generate_key()
    {
        $sql = "select generate_custom_pk(1) novoUser";
        $query = $this->db->query($sql);
        return $query->getFirstRow();
    }

    public function autenticarAcesso($dataValidate)
    {
        helper('session');
        if (!is_null($dataValidate) and !empty($dataValidate) and isset($dataValidate['email']) and isset($dataValidate['password'])) :
            $dataUser = $this->where('email', $dataValidate['email'])->first();
            if (!is_null($dataUser) and !empty($dataUser) and password_verify($dataValidate['password'], $dataUser->password)) {
                $this->modelGestaoAcesso = new \App\Models\GestaoAcessoModel();
                $this->modelGrupoAcesso = new \App\Models\GrupoAcessoModel();
                $this->modelFolders = new \App\Models\ArmazenamentoModel();
                $acessosUser = $this->modelGestaoAcesso->where('utilizador', $dataUser->idutilizador)->findAll();
                $idAcesso = array_map(function ($element) {
                    return $element['grupoAcesso'];
                }, $acessosUser);
                $listaAcessos = array_map(function ($element) {
                    return array(
                        'utilizador' => $element['utilizador'],
                        'grupoAcesso' => $element['grupoAcesso'],
                        'visualizar' => $element['visualizar'],
                        'transferir' => $element['transferir'],
                        'mover' => $element['mover'],
                        'copiar' => $element['copiar'],
                        'eliminar' => $element['eliminar'],
                        'editar' => $element['editar']
                    );
                }, $acessosUser);
                $grupoAcesso = $this->modelGrupoAcesso->whereIn('idgrupoAcesso', $idAcesso)->findAll();
                $grupoAcesso = array_map(function ($element) {
                    return array(
                        'idgrupoAcesso' => $element->idgrupoAcesso,
                        'icon' => $element->icon,
                        'descricao' => $element->descricao
                    );
                }, $grupoAcesso);
                return [
                    'identificador' => $dataUser->idutilizador,
                    'nome' => $dataUser->nome,
                    'email' => $dataUser->email,
                    'avatar' => $dataUser->avatar,
                    'perfil' => $dataUser->perfil,
                    'codigosAcessos' => $idAcesso,
                    'acessosUser' => $listaAcessos,
                    'grupoAcesso' => $grupoAcesso,
                    'listFolders' => $this->modelFolders->whereIn('grupoAcesso', $idAcesso)->findAll()
                ];
            }
        endif;
        return null;
    }
}
