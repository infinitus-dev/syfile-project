<?php

?>

<!DOCTYPE html>
<!--
Estrutura Principal Paginas
-->
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= isset($tituloPage) ? $tituloPage : 'SyFile' ?></title>
    <?= $this->include('layout/library-css') ?>
    <!-- icheck bootstrap -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>">

</head>

<body class="hold-transition <?= isset($classPage) ? $classPage : 'sidebar-mini' ?>">
    <div class="login-box">
        <div class="login-logo">
            <a href="javascript:void(0)"><img class="img-fluid" src="<?= base_url('assets/img/logo-name.png') ?>"></a>
        </div>
        <!-- /.login-logo -->
        <div class="card callout callout-warning">
            <div class="card-body login-card-body">
                <div class="">
                    <p class="login-box-msg">Entre com suas credências pra iniciar sua sessão</p>
                    <?= form_open(base_url('utilizador/autenticacao/validar-credencias'), array('id' => 'formAutenticacao')) ?>
                    <div class="input-group mb-3">
                        <input type="email" class="form-control" name="email" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" class="form-control" name="psw" placeholder="Palavra-passe">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Lembrar-me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Entrar</button>
                        </div>
                        <!-- /.col -->
                    </div>
                    <?= form_close() ?>

                    <p class="mb-1">
                        <a href="<?= base_url('utilizador/autenticacao/recuperar-credencias') ?>">Não Lembro a minha
                            Palavra-passe</a>
                    </p>
                </div>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->
    <?= $this->include('layout/library-js') ?>
</body>

</html>