<?php

/** @var ArrayObject $accessGroups */
/** @var ArrayObject $permitions */

$colors = json_decode(file_get_contents(WRITEPATH . "/help_files/colors.json"))->acessGroupColour;

?>
<?= $this->extend('layout/main') ?>
<?= $this->section('plugins-css') ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?= base_url('assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css') ?>">
<?= $this->endSection() ?>
<?= $this->section('content-page') ?>
<div class="container-fluid mt-3">
    <div class="card">
        <div class="card-header">
            <h5 class="card-title">Registo de Utilizador</h5>
            <div class="card-tools">
                <button class="btn btn-outline-primary" title="Registo de Grupo de Acessos" data-toggle="modal" data-target="#modal-registar-grupoacesso" title="Regitar novo Grupo de Acesso"><i class="fa fa-users"></i></button>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-9 col-sm-11 mx-auto">
                    <div class="card card-primary card-outline">
                        <div class="card-body box-profile">
                            <div class="text-center">
                                <img class="profile-user-img img-fluid img-circle" src="<?= base_url('assets/dist/img/avatar-add.png') ?>" alt="User profile picture">
                            </div>

                            <?= form_open(base_url('utilizadores/novo-registo/inserir'), array('id' => 'formRegistarUtilizador')) ?>
                            <fieldset class="border mb-1 border-secondary rounded p-2">
                                <legend class="text-info small">Identificação</legend>
                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="nomeCompleto">Nome</label>
                                            <input type="text" class="form-control" id="nomeCompleto" placeholder="insria o nome funcionario" autocomplete="off" autofocus name="nome" pattern="[A-Za-z\s]{3,}" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-sm-12">
                                        <div class="form-group">
                                            <label for="email">Correio Elétronico</label>
                                            <input type="email" class="form-control" name="email" id="email" autocomplete="off" placeholder="insria o email" required>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                            <hr class="divider">
                            <fieldset class="border mb-1 border-secondary rounded p-2">
                                <legend class="text-info small">Perfil Utilizador</legend>
                                <div class="row">
                                    <div class="col-6 form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" checked value="utilizador" name="perfil" id="perfil">
                                            <label for="perfil" class="small">
                                                Utilizador
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-6 form-group clearfix">
                                        <div class="icheck-primary d-inline">
                                            <input type="radio" value="administrador" name="perfil" id="perfil1">
                                            <label for="perfil1" class="small">
                                                Administrador
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </fieldset>
                            <hr class="divider">
                            <fieldset class="border mb-2 border-secondary rounded p-2">
                                <legend class="text-info small">Configuração de Acessos</legend>
                                <div class="row">
                                    <?php
                                    foreach ($accessGroups

                                        as $indexGroup => $accessGroup) :
                                    ?>
                                        <div class="col">
                                            <div class="info-box shadow">
                                                <div class="info-box-icon bg-<?= $accessGroup->color ?>">
                                                    <i class="<?= $accessGroup->icon ?>"></i>
                                                </div>

                                                <div class="info-box-content linesAcessos">
                                                    <div class="icheck-success d-inline grupoAcesso">
                                                        <input type="checkbox" name="grupoAcesso[]" value="<?= $accessGroup->idgrupoAcesso ?>" checked id="grupoAcesso<?= $indexGroup ?>">
                                                        <label for="grupoAcesso<?= $indexGroup ?>" class="info-box-text h5">
                                                            <?= $accessGroup->descricao ?>
                                                        </label>
                                                    </div>
                                                    <div class="row">
                                                        <?php
                                                        foreach ($permitions as $indexPermissao => $permition) :
                                                        ?>
                                                            <div class="col-4 mr-1 form-group clearfix">
                                                                <div class="icheck-primary d-inline linesPermissoes">
                                                                    <input type="checkbox" checked value="sim" name="permissoes[<?= $accessGroup->idgrupoAcesso ?>][<?= $permition->atributoBd ?>]" id="<?= $permition->atributoBd . strval($indexGroup) . strval($indexPermissao) ?>">
                                                                    <label for="<?= $permition->atributoBd . strval($indexGroup) . strval($indexPermissao) ?>" class="small">
                                                                        <?= $permition->descricao ?>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        <?php
                                                        endforeach;
                                                        ?>
                                                    </div>
                                                </div>
                                                <!-- /.info-box-content -->
                                            </div>
                                            <!-- /.info-box -->
                                        </div>

                                    <?php
                                    endforeach;
                                    ?>
                                </div>
                            </fieldset>
                            <div class="col-md-2 col-sm-6 mx-auto">
                                <button type="submit" class="btn btn-outline-success btn-block"><b>Salvar</b></button>
                            </div>
                            <?= form_close() ?>
                        </div>
                        <!-- /.card-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>



</div>
<div class="modal fade" id="modal-registar-grupoacesso">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Registo de Grupo de Acesso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open(base_url('utilizadores/grupo-acesso/inserir'), ['id' => 'formRegistarGrupoAcesso']) ?>
                <div class="form-group col-9">
                    <?= form_label('Descrição do Grupo', 'descricao') ?>
                    <?= form_input('descricao', '', ['id' => 'descricao', 'class' => 'form-control']) ?>
                </div>
                <div class="col-12">
                    <div class="card card-secondary">
                        <div class="card-header">
                            <h3 class="card-title">Icone do Grupo</h3>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <?php
                                foreach ($colors as $indexColor => $color) :
                                ?>
                                    <div class="col-2 form-group clearfix">
                                        <div class="icheck-concrete d-inline">
                                            <input type="radio" name="color" id="color<?= $indexColor ?>" value="<?= $color ?>">
                                            <label for="color<?= $indexColor ?>">
                                                <div class="bg-<?= $color ?> elevation-2" style="width: 40px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                            </label>
                                        </div>
                                    </div>
                                <?php
                                endforeach;
                                ?>
                            </div>
                            <hr class="ui-menu-divider">
                            <div class="row">
                                <div class="col-2 form-group clearfix">
                                    <div class="icheck-primary d-inline">
                                        <input type="radio" name="icone" value="fas fa-dot-circle" checked id="radioPrimary">
                                        <label for="radioPrimary">
                                            <i class="fas fa-dot-circle"></i>
                                        </label>
                                    </div>
                                </div>
                                <?php
                                foreach ($icones as $key => $icon) :
                                ?>
                                    <!-- radio -->
                                    <div class="col-2 form-group clearfix">

                                        <div class="icheck-primary d-inline">
                                            <input type="radio" id="radioPrimary<?= $key ?>" value="<?= $icon->class ?>" name="icone">
                                            <label for="radioPrimary<?= $key ?>">
                                                <i class="<?= $icon->class ?>"></i>
                                            </label>
                                        </div>
                                    </div>
                                <?php
                                endforeach;
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" hidden id="btnSubmitFormRegistar">inserir</button>
                <?= form_close() ?>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick="$('#btnSubmitFormRegistar').trigger('click')"><i class="fas fa-save"></i> Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?= $this->endSection() ?>
<?= $this->section('plugins-js') ?>
<script src="<?= base_url('assets/dist/js/registoUtilizador.js') ?>"></script>
<?= $this->endSection() ?>