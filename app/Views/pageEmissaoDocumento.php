<?= $this->extend('layout/main') ?>
<?= $this->section('plugins-css') ?>
<!-- Ionicons -->
<link rel="stylesheet" href="<?= base_url('assets/plugins/summernote/summernote-bs4.min.css') ?>">
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/plugins/select2/css/select2.min.css') ?>">
<?= $this->endSection() ?>
<?= $this->section('content-page') ?>
<div class="container-fluid mt-2">
    <div class="row">
        <div class="col-md-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Tipo de Documentos</h3>
                </div>
                <div class="card-body p-0">
                    <ul class="nav nav-tabs flex-column" role="tablist">
                        <li class="nav-item active">
                            <a href="#composicaoDocumento" id="composicaoDocumento-tab" class="nav-link active" data-toggle="pill" role="tab" aria-controls="composicaoDocumento" aria-selected="true">
                                <i class="fas fa-edit"></i> Compor
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="#declaracaoFuncionario" id="declaracaoFuncionario-tab" class="nav-link" data-toggle="pill" role="tab" aria-controls="declaracaoFuncionario" aria-selected="true">
                                <i class="fa fa-paperclip"></i> Declaração de Funcionario
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="custom-content-below-tabContent">
                <div class="tab-pane fade show active" role="tabpanel" id="composicaoDocumento" aria-labelledby="composicaoDocumento-tab">
                    <?= form_open(base_url(), ['id' => 'formComporDocumentacao']) ?>
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" id="btnPrevDocumento" title="Previzualizar Documento"><i class="fa fa-eye"></i></button>
                            </div>
                            <h3 class="card-title">Compor Documento</h3>
                        </div>
                        <div class="card-body">

                            <div class="form-group">
                                <div class="select2-purple">
                                    <select class="select2" multiple="multiple" data-placeholder="Destinatario:" data-dropdown-css-class="select2-purple" style="width: 100%;">
                                        <option>Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <input class="form-control" placeholder="Subject:">
                            </div>
                            <div class="form-group">
                                <textarea id="compose-textarea" class="form-control" style="height: 300px">
                                </textarea>
                            </div>

                        </div>
                        <div class="card-footer">
                            <div class="float-right">
                                <button type="button" class="btn btn-secondary"><i class="fa fa-save"></i> Salvar Rascunho</button>
                                <button type="submit" class="btn btn-success"><i class="fa fa-envelope"></i> Enviar</button>
                            </div>
                            <button type="reset" class="btn btn-warning"><i class="fa fa-eraser"></i> Descartar</button>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
                <div class="tab-pane fade" role="tabpanel" id="declaracaoFuncionario" aria-labelledby="declaracaoFuncionario-tab">
                    <div class="card card-secondary card-outline">
                        <div class="card-header">
                            <h4 class="card-title">Declaração de Funcionario</h4>
                        </div>
                        <div class="card-body">
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<?= $this->endSection(); ?>
<?= $this->section('plugins-js') ?>
<!-- Summernote -->
<script src="<?= base_url('assets/plugins/summernote/summernote-bs4.min.js') ?>"></script>
<!-- Select2 -->
<script src="<?= base_url('assets/plugins/select2/js/select2.full.min.js') ?>"></script>
<script>
    $(function() {
        //Add text editor
        $('#compose-textarea').summernote({
            height: (window.innerHeight * 0.2)
        });
        //Initialize Select2 Elements
        $('.select2').select2()

        //Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        });

        $("#btnPrevDocumento").click(function(e) {
            e.preventDefault();
            console.log($("#compose-textarea").val());
            let content = $("#compose-textarea").val();
            window.open(`${base_url}/documentacao/previsualizar-documento?conteudo=${content}`, '_blank', `scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,
width=${(window.innerWidth - (window.innerHeight * 0.2))},height=${(window.innerHeight - (window.innerHeight * 0.2))},left=100,top=100`)

        });
    })
</script>
<?= $this->endSection(); ?>