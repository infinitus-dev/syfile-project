<div class="card card-primary card-outline">
    <div class="card-body box-profile">
        <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="<?= base_url(isset($dadosUtilizador) ? 'assets/dist/img/avatars-users/' . $dadosUtilizador->avatar : 'assets/dist/img/avatar.png') ?>" alt="User profile picture">
        </div>

        <h3 class="profile-username text-center"><?= $dadosUtilizador->nome ?></h3>

        <p class="text-muted text-center"><?= $dadosUtilizador->email ?></p>
        <p class="text-center">
            <?php
            foreach ($grupoAcesso as $acesso) :
            ?>
                <i class="<?= $acesso->icon ?> fa-2x mr-2" title="<?= $acesso->descricao ?>"></i>
            <?php
            endforeach;
            ?>
        </p>

        <div class="scroll-smooth" style="max-height: 250px; overflow-y: scroll; overflow-x: hidden;">
            <ul class="list-group list-group-unbordered">
                <?php
                foreach ($listFolders as $myFolder) :
                ?>
                    <li class="list-group-item">
                        <a class="linesIcones btn-link text-muted" href="<?= base_url('armazenamento/detalhes/' . base64_encode(strtolower($myFolder->descricao)) . '/' . base64_encode(GENERAL_PATH . '/' . $myFolder->caminho)) ?>"> <b><?= $myFolder->descricao ?></b> <span class="float-right"><i class="fa fa-folder lineicon text-<?= $myFolder->color ?>"></i></span></a>
                    </li>
                <?php
                endforeach;
                ?>
            </ul>
        </div>
    </div>
    <!-- /.card-body -->
</div>