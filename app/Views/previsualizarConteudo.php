<!DOCTYPE html>
<!--
Estrutura Principal Paginas
-->
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SyFile - preview</title>
    <script>
        console.log('teste js');
    </script>
    <style>
        <?= file_get_contents('assets/dist/css/pdfPrev.css') ?>
    </style>
    <style>
        <?= file_get_contents('assets/dist/css/adminlte.min.css') ?>
    </style>

</head>

<body>
    <header>
        <table class="mt-2 table table-sm table-condensed" style="width: 100%;">
            <tr class="m-0 p-0">
                <th class="m-0 p-0" style="width: 25%;"><img src="<?= base_url('assets/img/98-98.svg') ?>" alt="Logo"></th>
                <td class="m-0 p-0" style="width: 40%;"></td>
                <td class="m-0 p-0" style="width: 35%;">
                    <table class="table table-sm table-condensed">
                        <tr class="m-0 p-0">
                            <th class="m-0 p-0 small">Data Emissão</th>
                            <td class="m-0 p-0 small"><?= date('Y-m-d') ?></td>
                        </tr>
                        <tr class="m-0 p-0">
                            <th class="m-0 p-0 small">Nº Documento</th>
                            <td class="m-0 p-0 small">DOC001/1023</td>
                        </tr>
                        <tr class="m-0 p-0">
                            <th class="m-0 p-0 small">Data Impressao</th>
                            <td class="small m-0 p-0"><?= date('Y-m-d') ?></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </header>
    <footer>
        <table class="w-100 table table-sm table-condensed">
            <tr class="m-0 p-0">
                <th class="w-75 p-0 m-0 small text-center">Documento gerado e emitido pela Syfile</th>
                <td class="w25 p-0 m-0 small">Operador: <?= $dataUser['nome'] ?></td>
            </tr>
            <tr class="m-0 p-0">
                <th class="m-0 p-0 small text-center" colspan="2">Cod. Autenticação: <?= base64_encode('12345645678965241') ?></th>
            </tr>
        </table>
    </footer>
    <main>
        <?= $content ?>

    </main>

    <!-- jQuery -->
    <script src="<?= base_url('assets/plugins/jquery/jquery.min.js') ?>"></script>
    <!-- Bootstrap 4 -->
    <script src="<?= base_url('assets/plugins/bootstrap/js/bootstrap.bundle.min.js') ?>"></script>
    <!-- AdminLTE App -->
    <script src="<?= base_url('assets/dist/js/adminlte.min.js') ?>"></script>

</body>

</html>