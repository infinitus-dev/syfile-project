<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Syfile - 404 Page Not Found</title>


	<!-- Google Font: Source Sans Pro -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
	<!-- Font Awesome Icons -->
	<link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css') ?>">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css') ?>">
	<!-- Swetalert-->
	<link href="<?= base_url('assets/plugins/sweetalert2/sweetalert2.min.css') ?>" rel="stylesheet">
</head>

<body class="hold-transition">
	<section class="content">
		<div class="error-page">
			<h2 class="headline text-warning"> 404</h2>

			<div class="error-content">
				<h3><i class="fas fa-exclamation-triangle text-warning"></i> Oops! Pagina não encontrada.</h3>
				<p>
					<?php if (!empty($message) && $message !== '(null)') : ?>
						<?= esc($message) ?>
					<?php else : ?>
						Desculpe! Não foi possivel encontrar a pagina que está a procura. Entretanto, poderá <a href="<?= base_url('') ?>">voltar ao inicio</a> ou submeter uma pesquisa!
					<?php endif ?>
				</p>
				<form class="search-form">
					<div class="input-group">
						<input type="text" name="search" class="form-control" placeholder="Pesquisar">

						<div class="input-group-append">
							<button type="submit" name="submit" class="btn btn-warning"><i class="fas fa-search"></i>
							</button>
						</div>
					</div>
					<!-- /.input-group -->
				</form>
			</div>
			<!-- /.error-content -->
		</div>
		<!-- /.error-page -->
	</section>
</body>

</html>