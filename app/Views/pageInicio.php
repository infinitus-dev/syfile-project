<?php

/** @var array $armazenamentos */
/** @var ArrayObject $pastasUtilizador */
?>
<?= $this->extend('layout/main') ?>
<?= $this->section('plugins-css') ?>
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<?= $this->endSection() ?>
<?= $this->section('content-page') ?>
<div class="container-fluid">
    <h5 class="mb-2">Box</h5>

    <div class="row mt-1">
        <?php
        foreach ($pastasUtilizador as $folder) {
            if (!verificarItem(GENERAL_PATH . '/' . $folder->caminho) or !is_null($folder->origem))
                continue;
            $f = new \App\Models\FolderModel(strtolower($folder->descricao), GENERAL_PATH . '/' . $folder->caminho);
        ?>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <!-- small card -->
                <div class="small-box  bg-<?= $folder->color ?> linesIcones">
                    <div class="inner">
                        <h4><?= $folder->descricao ?></h4>
                        <p style=" font-size: x-small">Ult. Modif.<?= $f->getUpdateData() ?></p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-folder lineicon"></i>
                    </div>
                    <a href="<?= base_url('armazenamento/detalhes/' . base64_encode(strtolower($folder->descricao)) . '/' . base64_encode(GENERAL_PATH . '/' . $folder->caminho)) ?>" class="small-box-footer">
                        Abrir <i class="fas fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>
        <?php
        }
        ?>
    </div>
</div>

<?= $this->endSection(); ?>
<?= $this->section('plugins-js') ?>

<?= $this->endSection(); ?>