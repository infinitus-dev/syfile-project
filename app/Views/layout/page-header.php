<?php
/** @var string $submoduloName */
/** @var string $currectModulo */
/** @var string $submoduloName */

?>
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark"><?= isset($currectModulo) ? $currectModulo : 'Default Name'?></h1>
            </div><!-- /.col -->
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="javascript:void(0)"><?= isset($currectModulo) ? $currectModulo : 'Default Name'?></a></li>
                    <li class="breadcrumb-item active"><?= isset($submoduloName) ? $submoduloName : 'Default Name'?></li>
                </ol>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container-fluid -->
</div>
