<?php

/** @var ArrayObject $dataUser */
/** @var ArrayObject $funcionalidades */
/** @var string $currectModulo */
/** @var string $currectSubModulo */
?>
<aside class="main-sidebar sidebar-dark-orange elevation-4">
    <!-- Brand Logo -->
    <a href="<?= base_url() ?>" class="brand-link">
        <img src="<?= base_url('assets/img/logo-name.png') ?>" alt="SyFile Logo" class="brand-image img-rounded" style="opacity: .8">
        <br>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="<?= base_url(isset($dataUser) ? 'assets/dist/img/avatars-users/' . $dataUser['avatar'] : 'assets/dist/img/avatar.png') ?>" class="img-circle elevation-2" alt="User Image">
            </div>
            <div class="info">
                <a href="javascript:void(0)" title="Perfil de Utilizador" onclick="servicoModalAjax('<?= base_url('utilizadores/ver-perfil/' . $dataUser['identificador']) ?>','#modalPersolaUserInfo', ()=>{}, 'Perfil de Utilizador')" class="d-block"><?= isset($dataUser) ? $dataUser['nome'] : 'Administrador' ?></a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills text-sm nav-sidebar nav-child-indent flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <?php
                if (isset($funcionalidades) and !empty($funcionalidades) and !is_null($funcionalidades)) :
                    $control = [];
                    foreach ($funcionalidades as $funcionalidade) :
                        if (!in_array($funcionalidade->modulo, $control)) :
                ?>
                            <li class="nav-item has-treeview <?= $funcionalidade->modulo === $currectModulo ? 'menu-open' : '' ?>">
                                <a href="javascript:void(0)" class="nav-link <?= $funcionalidade->modulo === $currectModulo ? 'active' : '' ?>">
                                    <i class="nav-icon <?= $funcionalidade->icon ?>"></i>
                                    <p>
                                        <?= $funcionalidade->modulo ?>
                                        <i class="right fas fa-angle-left"></i>
                                    </p>
                                </a>
                                <ul class="nav nav-treeview">
                                    <?php
                                    foreach ($funcionalidades as $submodulo) :
                                        if ($submodulo->modulo === $funcionalidade->modulo) :
                                            $control[] = $funcionalidade->modulo;
                                    ?>
                                            <li class="nav-item">
                                                <a href="<?= base_url($submodulo->link) ?>" class="nav-link <?= $submodulo->idfuncionalidade === $currectSubModulo ? 'active' : '' ?>">
                                                    <i class=" <?= $submodulo->idfuncionalidade === $currectSubModulo ? 'fas fa-dot-circle' : 'far fa-circle' ?> nav-icon"></i>
                                                    <p><?= $submodulo->descricao ?></p>
                                                </a>
                                            </li>
                                    <?php
                                        endif;
                                    endforeach;
                                    ?>
                                </ul>
                            </li>
                <?php
                        endif;
                    endforeach;
                endif;
                ?>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>