<?php

?>

<footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline" style="font-size: xx-small">
        <strong style="font-size: xx-small">Copyright &copy; 2014-2020 Template by <a href="https://adminlte.io">AdminLTE.io</a>.</strong> All rights reserved.

    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2020-<?= date('Y') ?> Powered By <a href="https://infinitustctc.com">Infinitus</a>.</strong> Todos os Direitos Reservados.
</footer>