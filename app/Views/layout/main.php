<?php

/** @var $tituloPage */
/** @var $classPage */
/** @var $dataUser */
$colors = json_decode(file_get_contents(WRITEPATH . "/help_files/colors.json"))->acessGroupColour;
?>

<!DOCTYPE html>
<!--
Estrutura Principal Paginas
-->
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= isset($tituloPage) ? $tituloPage : 'SyFile' ?></title>
    <script>
        const base_url = '<?= base_url() ?>';
    </script>
    <?= $this->include('layout/library-css') ?>
    <?= $this->renderSection("plugins-css") ?>
</head>

<body class="hold-transition <?= isset($classPage) ? $classPage : 'sidebar-mini' ?>">
    <div class="wrapper">
        <!-- Navbar -->
        <?= $this->include('layout/navbar') ?>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <?= $this->include('layout/sidebar') ?>
        <!-- /. main Sidebar Container -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <?= $this->include('layout/page-header') ?>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="d-flex flex-row-reverse">
                    <div class="btn-group">
                        <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-criarFolder"><i class="fas fa-folder-plus"></i></button>
                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#modal-uploadArchive"><i class="fas fa-upload"></i></button>
                    </div>
                </div>
                <?= $this->renderSection("content-page") ?>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?= $this->include('layout/footer') ?>
    </div>
    <!-- ./wrapper -->
    <div class="modal fade" id="modal-criarFolder">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Criar Nova Pasta</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open(base_url('armazenamento/pasta/criar-nova-pasta'), ['class' => 'col-10 mx-auto', 'id' => 'formCriarFolder']) ?>
                    <div class="form-group">
                        <label for="folder-name">Nome Pasta</label>
                        <input type="text" name="folder-name" id="folder-name" class="form-control" required>
                    </div>
                    <div class="row">
                        <?php
                        foreach ($colors as $indexColor => $color) :
                        ?>
                            <div class="col-2 form-group clearfix">
                                <div class="icheck-concrete d-inline">
                                    <input type="radio" name="colorFolder" id="colorFolder<?= $indexColor ?>" value="<?= $color ?>">
                                    <label for="colorFolder<?= $indexColor ?>">
                                        <div class="bg-<?= $color ?> elevation-2" style="width: 30px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                    </label>
                                </div>
                            </div>
                        <?php
                        endforeach;
                        ?>
                    </div>
                    <div class="form-group">
                        <label for="grupoAcesso">Grupo Acesso</label>
                        <select class="custom-select" id="grupoAcesso" name="grupoAcesso" required>

                            <?php
                            foreach ($dataUser['grupoAcesso'] as $itemGrupoAcesso) :
                            ?>
                                <option value="<?= $itemGrupoAcesso['idgrupoAcesso'] ?>"><?= $itemGrupoAcesso['descricao'] ?></option>
                            <?php
                            endforeach;
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label for="path">Caminho Destino</label>
                        <select class="form-control" id="path" name="path" required>
                            <option value="<?= '/armazenamento' ?>">Raiz</option>
                            <?php
                            foreach ($dataUser['listFolders'] as $itemFolder) :
                            ?>
                                <option value="<?= $itemFolder->idarmazenamento . '||' . $itemFolder->caminho ?>"><?= $itemFolder->descricao ?></option>
                            <?php
                            endforeach;
                            ?>

                        </select>
                    </div>
                    <button type="submit" id="btnFormFolder" hidden>inserir</button>
                    <?= form_close() ?>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-success" onclick="$('#btnFormFolder').trigger('click')"> <i class="fa fa-folder-plus"></i> Criar</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade" id="modal-uploadArchive">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Carregar Ficheiros</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open_multipart(base_url('armazenamento/upload/carregar-ficheiro'), array('id' => 'formUploadArchive')) ?>
                    <div class="form-group">
                        <label for="pathUpload">Caminho Destino</label>
                        <select class="form-control" id="pathUpload" name="path" required>
                            <?php
                            foreach ($dataUser['listFolders'] as $itemFolder) :
                            ?>
                                <option value="<?= $itemFolder->idarmazenamento . '||' . $itemFolder->caminho ?>"><?= $itemFolder->descricao ?></option>
                            <?php
                            endforeach;
                            ?>

                        </select>
                    </div>
                    <button type="submit" id="btnFormUpload" hidden>inserir</button>
                    <div class="form-group">
                        <label for="filesUpload">Ficheiros</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" name="file[]" multiple id="filesUpload">
                                <label class="custom-file-label" for="exampleInputFile">Carregar Ficheiro</label>
                            </div>
                        </div>
                    </div>
                    <?= form_close() ?>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-success" onclick="$('#btnFormUpload').trigger('click')"> <i class="fa fa-upload"></i> Carregar</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalPersolaUserInfo">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4></h4>
                </div>
                <div class="modal-body">
                    
                </div>

            </div>
        </div>

    </div>

    <!-- REQUIRED SCRIPTS -->
    <?= $this->include('layout/library-js') ?>
    <?= $this->renderSection("plugins-js") ?>
</body>

</html>