<?php
// Bibliotecas Default de CSS
?>

<!-- Google Font: Source Sans Pro -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
<!-- Font Awesome Icons -->
<link rel="stylesheet" href="<?= base_url('assets/plugins/fontawesome-free/css/all.min.css')?>">
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url('assets/dist/css/adminlte.min.css')?>">
<!-- Swetalert-->
<link href="<?= base_url('assets/plugins/sweetalert2/sweetalert2.min.css')?>" rel="stylesheet">