<?php

/** @var string $folderName */
/** @var string $pathFolder */
/** @var  ArrayObject $armazenamento */
/** @var  ArrayObject $dataUser */
$folder = new \App\Models\FolderModel($folderName, $pathFolder);
$modelFicheiro = new \App\Models\FicheiroModel();
$armazenamentoModel = new \App\Models\ArmazenamentoModel();
$colors = json_decode(file_get_contents(WRITEPATH . "/help_files/colors.json"))->acessGroupColour;
if (is_null($armazenamento->origem) and empty($armazenamento->origem)) :
    $folderRaiz = null;
else :
    $folderRaiz = $armazenamentoModel->where('idarmazenamento', $armazenamento->origem)->first();
endif;
$permissoes = null;
foreach ($dataUser['acessosUser'] as $item) {
    if ($item['grupoAcesso'] === $acessoPasta) {
        $permissoes = $item;
        break;
    }
}
?>
<?= $this->extend('layout/main') ?>
<?= $this->section('content-page') ?>
<!-- Default box -->
<div class="card mt-2">
    <div class="card-header">
        <h3 class="card-title text-capitalize"><?= utf8_decode($folderName) ?></h3>

        <div class="card-tools">
            <a class="btn btn-sm btn-light" title="voltar <?= ($folderRaiz ? $folderRaiz->descricao : 'Box - Inicio') ?>" href="<?= ((!is_null($folderRaiz) and !empty($folderRaiz)) ?  base_url('armazenamento/detalhes/' . base64_encode(strtolower($folderRaiz->descricao)) . '/' . base64_encode(GENERAL_PATH . '/' . $folderRaiz->caminho)) : base_url('')) ?>">
                <i class="fas fa-undo"></i>
            </a>
            <button type="button" class="btn btn-sm btn-primary" data-target="#modalLocalUpload" data-toggle="modal" title="Carregar ficheiro em <?= $folderName ?>">
                <i class="fas fa-file-upload"></i></button>
            <button type="button" class="btn btn-sm btn-secondary" data-target="#modalLocalCreateFolder" data-toggle="modal" title="Criar sub-pasta em <?= $folderName ?>">
                <i class="fas fa-folder-plus"></i>
            </button>
        </div>
    </div>
    <div class="card-body p-0">
        <table class="table table-striped projects">
            <thead>
                <tr>
                    <th style="width: 1%">
                        #
                    </th>
                    <th style="width: 30%">
                        Nome
                    </th>
                    <th style="width: 10%">
                        Tam. Memoria
                    </th>
                    <th>
                        Ult. Modificação
                    </th>
                    <th style="width: 8%" class="text-center">
                        Estado
                    </th>
                    <th style="width: 30%">
                    </th>
                </tr>
            </thead>
            <tbody>
                <?php
                $total['memoria'] = 0;
                $total['pastas'] = 0;
                $total['ficheiros'] = 0;
                foreach ($folder->getContent() as $item) :
                ?>
                    <tr>
                        <td>
                            #
                        </td>
                        <td>
                            <?php
                            if ($item['type'] != 'file') :
                                $total['pastas'] += 1;
                            ?>
                                <a href="<?= base_url('armazenamento/detalhes/' . base64_encode(strtolower($item['name'])) . '/' . base64_encode($item['path'])) ?>">
                                    <?= $item['name'] ?>
                                </a>
                            <?php
                            else :
                                $total['memoria'] += intval($item['size']);
                                $total['ficheiros'] += 1;
                                echo $item['name'];
                            ?>
                            <?php
                            endif;
                            ?>
                        </td>
                        <td>
                            <?= $item['type'] == 'file' ? fm_get_filesize(intval($item['size'])) : 'Pasta' ?>
                        </td>
                        <td>
                            <?= date('D d-M-Y H:i:s', fileatime($item['path'])) ?>
                        </td>
                        <td class="project-state">
                            <?php
                            $ctrl = array();
                            if ($item['type'] == 'file') :
                                $ctrl = $modelFicheiro->where('caminho', str_replace(GENERAL_PATH . '/', '', $item['path'] . '/' . $item['name']))->first();
                            ?>
                            <?php
                            else :
                                $ctrl = $armazenamentoModel->where('caminho', str_replace(GENERAL_PATH . '/', '', $item['path']))->first();
                            ?>
                            <?php
                            endif;
                            ?>
                            <span class="badge badge-<?= (!empty($ctrl) and !is_null($ctrl)) ? 'success' : 'warning' ?>"><?= (!empty($ctrl) and !is_null($ctrl)) ? 'Success' : 'Warning' ?></span>
                        </td>
                        <td class="project-actions text-right">
                            <?php
                            switch ($item['type']):
                                case 'file':
                                    if (empty($permissoes) or (!empty($permissoes) and $permissoes['visualizar'] === 'sim')) :
                                        $url = base_url('armazenamento/ficheiro/detalhes/' . base64_encode(strtolower($item['name'])) . '/' . base64_encode($item['path']));
                            ?>
                                        <a class="btn btn-primary btn-sm" href="javascript:window.open('<?= $url . '?type=v' ?>','mywindowtitle','width=500,height=150')">
                                            <i class="fas fa-eye">
                                            </i>
                                        </a>
                                    <?php
                                    endif;
                                    if (empty($permissoes) or (!empty($permissoes) and $permissoes['transferir'] === 'sim')) :
                                        $urlDwo = base_url('armazenamento/ficheiro/download/' . base64_encode(strtolower($item['name'])) . '/' . base64_encode($item['path']));
                                    ?>
                                        <a class="btn btn-primary btn-sm" href="<?= $urlDwo ?>">
                                            <i class="fas fa-download">
                                            </i>
                                        </a>
                                    <?php
                                    endif;
                                    break;
                                case 'folder':
                                    ?>
                                    <a class="btn btn-primary btn-sm" href="#">
                                        <i class="fa fa-file-archive">
                                        </i>
                                    </a>
                                <?php
                                    break;
                            endswitch;
                            if (empty($ctrl)) :
                                ?>
                                <button class="btn btn-info btn-sm" onclick="catalogacao('<?= str_replace(GENERAL_PATH, '', $item['path']) ?>','<?= $item['name'] ?>','<?= $item['type'] ?>','<?= $item['type'] == 'file' ? fm_get_file_icon_class($item['path'] . '/' . $item['name']) : 'fas fa-folder' ?>')">
                                    <i class="fas fa-cogs">
                                    </i>
                                </button>
                            <?php
                            endif;
                            if (empty($permissoes) or (!empty($permissoes) and $permissoes['editar'] === 'sim')) :
                            ?>
                                <button class="btn btn-info btn-sm" onclick="edicaoFile('<?= str_replace(GENERAL_PATH, '', $item['path']) ?>','<?= $item['name'] ?>','<?= $item['type'] == 'file' ? fm_get_file_icon_class($item['path'] . '/' . $item['name']) : 'fas fa-folder' ?>','<?= ($item['type'] == 'file' and !empty($ctrl)) ? $ctrl->idficheiro : (($item['type'] == 'folder' and !empty($ctrl)) ? $ctrl->idarmazenamento : 0) ?>')">
                                    <i class="fas fa-pencil-alt">
                                    </i>
                                </button>
                            <?php
                            endif;
                            if (empty($permissoes) or (!empty($permissoes) and $permissoes['eliminar'] === 'sim')) :
                            ?>
                                <a class="btn btn-danger btn-sm" href="javascript:void(0)" onclick="eliminarFilesFoldes('<?= str_replace(GENERAL_PATH, '', $item['path']) ?>','<?= $item['name'] ?>','<?= ($item['type'] == 'file' and !empty($ctrl)) ? $ctrl->idficheiro : (($item['type'] == 'folder' and !empty($ctrl)) ? $ctrl->idarmazenamento : 0) ?>','<?= $item['type'] == 'file' ? fm_get_file_icon_class($item['path'] . '/' . $item['name']) : 'fas fa-folder text-blue' ?>','<?= $item['type'] ?>')">
                                    <i class="fas fa-trash">
                                    </i>
                                </a>
                            <?php
                            endif;
                            if (empty($permissoes) or (!empty($permissoes) and ($permissoes['mover'] === 'sim' or $permissoes['copiar'] === 'sim'))) :
                            ?>
                                <button class="btn btn-info btn-sm" onclick="copiarMoverFiles('<?= str_replace(GENERAL_PATH, '', $item['path']) ?>', '<?= $item['name'] ?>','<?= $item['type'] == 'file' ? fm_get_file_icon_class($item['path'] . '/' . $item['name']) : 'fas fa-folder text-blue' ?>','<?= ($item['type'] == 'file' and !empty($ctrl)) ? $ctrl->idficheiro : (($item['type'] == 'folder' and !empty($ctrl)) ? $ctrl->idarmazenamento : 0) ?>','<?= $item['type'] ?>')">
                                    <i class="fas fa-copy">
                                    </i>
                                </button>
                            <?php
                            endif;
                            ?>
                        </td>
                    </tr>

                <?php
                endforeach;
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td></td>
                    <td colspan="5">Total Memoria: <?= fm_get_filesize($total['memoria']) ?>,
                        ficheiros: <?= $total['ficheiros'] ?>, pastas: <?= $total['pastas'] ?></td>
                </tr>
            </tfoot>
        </table>
    </div>
    <!-- Modal Catalogação de Ficheiros e Pastas sem Registo na BD-->
    <div class="modal fade" id="modalCatalogarItens">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Catalogação de Ficheiros</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <?= form_open(base_url('armazenamento/ficheiro/catalogar-novos-ficheiros'), array('id' => 'formCatalogarFiles')) ?>
                    <div class="row">
                        <div class="col-4">
                            <div class="icon text-center align-middle" id="iconContent"></div>
                        </div>
                        <div class="col-8">
                            <div class="form-group">
                                <label>Item</label>
                                <div class="input-group mb-3">
                                    <input type="text" name="fileName" class="form-control" readonly id="fileNameCtl" required>
                                    <input type="hidden" name="oldName" id="oldName">
                                    <input type="hidden" name="origemFile" id="origemFile" required>
                                    <input type="hidden" name="typeFile" id="typeFile" required>

                                    <div class="input-group-append">
                                        <span class="input-group-text" id="btnEditName"><i class="fas fa-edit"></i></span>
                                    </div>
                                </div>

                            </div>
                            <div class="form-group">
                                <label id="destinoCtl">Destino: </label>
                                <select class="form-control" name="pathFile" required>
                                    <?php
                                    foreach ($pastasUtilizador as $itemFolder) :
                                    ?>
                                        <option value="<?= $itemFolder->idarmazenamento . '||' . $itemFolder->caminho ?>">
                                            <?= $itemFolder->descricao ?></option>
                                    <?php
                                    endforeach;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <button type="submit" hidden id="btnSbmtCatalogarFiles">insert</button>
                    <?= form_close() ?>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#btnSbmtCatalogarFiles').trigger('click')">Salvar
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Modal Renomear Pastas  e Ficheiros -->
    <div class="modal fade" id="modalEditarNome">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Alterar Nome Ficheiros/Pastas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= form_open(base_url('armazenamento/ficheiro/editar-nome-ficheiro'), ['id' => 'formEditarFile']) ?>
                    <div class="info-box">
                        <span class="info-box-icon bg-info editContentIcon"><i class="far fa-folder"></i></span>
                        <div class="info-box-content">
                            <div class="form-group">
                                <label>Nome de Ficheiro</label>
                                <input type="text" class="form-control" name="newName" id="nomeFicheiroEditar" maxlenght="255" require>
                                <input type="hidden" id="oldNameFileEdit" name="oldNameFile">
                                <input type="hidden" id="pathFileEdit" name="path">
                                <input type="hidden" name="idItem" id="idItemEdit" required>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <button type="submit" id="btnFormEditarFiles" hidden>inserir</button>
                    <?=
                    form_close();
                    ?>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#btnFormEditarFiles').trigger('click')">Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Copiar e Mover Ficheiros -->
    <div class="modal fade" id="modalCopiarMoverFiles">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mover/Copiar Ficheiros/Pastas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-11 col-sm-12 mx-md-auto">
                        <div class="info-box">
                            <span class="info-box-icon bg-light" id="iconMoveCopy"></span>
                            <div class="info-box-content">
                                <span class="info-box-number" style="font-size: x-small">Pasta Origem: <b id="contentFileMoveCopy"></b></span>
                                <span class="info-box-text text-capitalize" id="contentNameFileMoveCopy"></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <?=
                    form_open(base_url('armazenamento/ficheiro/mover-copiar-ficheiro'), ['id' => 'formCopiarMoverFiles']);
                    ?>
                    <input type="hidden" name="path" id="pathMoveCopy">
                    <input type="hidden" name="nameFile" id="nameFileMoveCopy">
                    <input type="hidden" name="idItem" id="idItemFileMoveCopy">
                    <input type="hidden" name="type" id="typeFileMoveCopy">
                    <input type="checkbox" name="isCopy" checked data-bootstrap-switch data-off-color="info" data-on-color="success" data-off-text='Mover' data-on-text='Copiar'>
                    <div class="form-group">
                        <label for="destinoFolder">Destino</label>
                        <select class="form-control" name="pathDestino" required>
                            <?php
                            foreach ($pastasUtilizador as $itemFolder) :
                            ?>
                                <option value="<?= $itemFolder->idarmazenamento . '||' . $itemFolder->caminho ?>">
                                    <?= $itemFolder->descricao ?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                    <button type="submit" id="btnformCopiarMoverFiles" hidden>inserir</button>
                    <?=
                    form_close();
                    ?>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#btnformCopiarMoverFiles').trigger('click')">Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Eliminar Ficheiros e Pastas -->
    <div class="modal fade" id="modalEliminarFilesFolders">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Eliminar Ficheiros/Pastas</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="col-md-11 col-sm-12 mx-md-auto">
                        <div class="info-box">
                            <span class="info-box-icon bg-light" id="iconDeleteFile"></span>
                            <div class="info-box-content">
                                <span class="info-box-text text-capitalize" id="contentNameFileDelete"></span>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    <?=
                    form_open(base_url('armazenamento/ficheiro/eliminar-ficheiros'), ['id' => 'formEliminarFilesFolders']);
                    ?>
                    <input type="hidden" name="idItem" id="idItemDelete">
                    <input type="hidden" name="nameFile" id="nameItemDelete">
                    <input type="hidden" name="pathItem" id="pathItemDelete">
                    <input type="hidden" name="type" id="typeItemDelete">
                    <div class="col-md-11 mx-md-auto col-sm-12">
                        <div class="btn-group mx-auto col-12 btn-group-toggle" data-toggle="buttons">
                            <label class="btn btn-warning active">
                                <input type="radio" name="typeOp" id="option_a1" value="reciclagem" autocomplete="off" checked="">
                                Enviar <br>Reciclagem
                            </label>
                            <label class="btn btn-warning">
                                <input type="radio" name="typeOp" id="option_a2" value="permanente" autocomplete="off">
                                Eliminar<br>
                                Permanentemente
                            </label>
                        </div>
                        <button type="submit" hidden id="btnFormEliminarFicheiros">inserir</button>
                    </div>
                    <?=
                    form_close();
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="button" class="btn btn-primary" onclick="$('#btnFormEliminarFicheiros').trigger('click')">Salvar
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalLocalUpload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Carregar Ficheiro em <?= $folderName ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open_multipart(base_url('armazenamento/upload/carregar-ficheiro'), array('id' => 'formLocalUploadArchive')) ?>
                <?= form_hidden('path', $armazenamento->idarmazenamento . '||' . $armazenamento->caminho) ?>
                <button type="submit" id="btnFormLocalUpload" hidden>inserir</button>
                <div class="form-group">
                    <label for="filesUpload">Ficheiros</label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="file[]" multiple id="localFilesUpload">
                            <label class="custom-file-label" for="localInputFile">Carregar Ficheiro</label>
                        </div>
                    </div>
                </div>
                <?= form_close() ?>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick="$('#btnFormLocalUpload').trigger('click')"> <i class="fa fa-upload"></i> Carregar</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalLocalCreateFolder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Criar nova pasta em <?= $folderName ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <?= form_open(base_url('armazenamento/pasta/criar-nova-pasta'), ['class' => 'col-10 mx-auto', 'id' => 'formCriarFolderLocal']) ?>
                <div class="form-group">
                    <label for="localFolder-name">Nome Pasta</label>
                    <input type="text" name="folder-name" id="localFolder-name" class="form-control" required>
                </div>
                <div class="row">
                    <?php
                    foreach ($colors as $indexColor => $color) :
                    ?>
                        <div class="col-2 form-group clearfix">
                            <div class="icheck-concrete d-inline">
                                <input type="radio" name="colorFolder" id="localColorFolder<?= $indexColor ?>" value="<?= $color ?>">
                                <label for="localColorFolder<?= $indexColor ?>">
                                    <div class="bg-<?= $color ?> elevation-2" style="width: 30px; height: 20px; border-radius: 25px; margin-right: 10px; margin-bottom: 10px; opacity: 0.8; cursor: pointer;"></div>
                                </label>
                            </div>
                        </div>
                    <?php
                    endforeach;
                    ?>
                </div>
                <div class="form-group">
                    <label for="localGrupoAcesso">Grupo Acesso</label>
                    <select class="custom-select" id="localGrupoAcesso" name="grupoAcesso" required>

                        <?php
                        foreach ($dataUser['grupoAcesso'] as $itemGrupoAcesso) :
                            if ($itemGrupoAcesso['idgrupoAcesso'] === $armazenamento->grupoAcesso) :
                        ?>
                                <option selected value="<?= $itemGrupoAcesso['idgrupoAcesso'] ?>"><?= $itemGrupoAcesso['descricao'] ?></option>
                        <?php
                            else :
                                continue;
                            endif;
                        endforeach;
                        ?>

                    </select>
                </div>
                <?= form_hidden('path', $armazenamento->idarmazenamento . '||' . $armazenamento->caminho) ?>
                <button type="submit" id="btnFormLocalFolder" hidden>inserir</button>
                <?= form_close() ?>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" class="btn btn-success" onclick="$('#btnFormLocalFolder').trigger('click')"> <i class="fa fa-folder-plus"></i> Criar</button>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('plugins-js') ?>
<!-- Bootstrap Switch -->
<script src="<?= base_url('assets/plugins/bootstrap-switch/js/bootstrap-switch.min.js') ?>"></script>
<script src="<?= base_url('assets/dist/js/folderDetalhe.js') ?>"></script>
<?= $this->endSection() ?>