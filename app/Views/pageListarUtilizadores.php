<?php

/** @var ArrayObject $utilizadores */
/** @var object $pager */
/** @var \app\Models\GestaoAcessoModel $gestaoAcesso */
?>
<?= $this->extend('layout/main') ?>
<?= $this->section('content-page') ?>

<!-- Default box -->
<div class="card card-solid mt-3">
    <div class="card-body pb-0">
        <div class="row d-flex align-items-stretch">
            <?php
            foreach ($utilizadores as $utilizador) :
                $acessos = $gestaoAcesso->join('grupoacessos', 'grupoacessos.idgrupoAcesso = grupoAcesso')->where('utilizador', $utilizador->idutilizador)->findAll();
            ?>
                <div class="col-12 col-sm-6 col-md-6 col-lg-6 col-xl-4 d-flex align-items-stretch">
                    <div class="card bg-light">
                        <div class="card-header text-muted border-bottom-0">
                            <?= $utilizador->nome ?>
                        </div>
                        <div class="card-body pt-0">

                            <div class="row">
                                <div class="col-7">
                                    <h6 class="lead small"><b><?= $utilizador->email ?></b></h6>
                                    <p class="text-muted text-sm"><b>Grupo de Acessos: </b>
                                        <?php
                                        if (isset($acessos[0])) :
                                            foreach ($acessos as $acesso) :
                                                echo $acesso['descricao'] . "; ";
                                            endforeach;
                                        else :
                                            echo $acessos['descricao'];
                                        endif;
                                        ?></p>
                                    <ul class="ml-4 mb-0 fa-ul text-muted">
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-lock"></i></span>
                                            <kbd>Identificação: </kbd><?= $utilizador->idutilizador ?>
                                        </li>
                                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span>
                                            <kbd>Data Registo</kbd><br><?= $utilizador->created_at ?>
                                        </li>
                                        <li class="small"> <span class="fa-li"><i class="fas fa-lg fa-calendar"></i></span>
                                            <kbd>Ultima Atualização</kbd><br><?= $utilizador->updated_at ?>

                                        </li>
                                    </ul>
                                </div>
                                <div class="col-5 text-center">
                                    <img src="<?= base_url('assets/dist/img/avatars-users/' . $utilizador->avatar) ?>" alt="user-avatar" class="img-circle img-fluid">
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <div class="text-right">
                                <a href="javascript:void(0)" onclick="servicoModalAjax('<?= base_url('utilizadores/ver-perfil/' . $utilizador->idutilizador) ?>','#modalPersolaUserInfo', ()=>{}, 'Perfil de Utilizador')" class="btn btn-sm btn-primary">
                                    <i class="fas fa-user"></i> Ver Perfil
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php
            endforeach;
            ?>

        </div>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        <?= $pager->links('pagina', 'default_profile') ?>
    </div>
</div>

<?= $this->endSection() ?>