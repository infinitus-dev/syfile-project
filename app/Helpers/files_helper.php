<?php
/**
 * Get CSS classname for file
 * @param string $path
 * @return string
 */
function fm_get_file_icon_class($path)
{
    // get extension
    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));

    switch ($ext) {
        case 'ico':
        case 'gif':
        case 'jpg':
        case 'jpeg':
        case 'jpc':
        case 'jp2':
        case 'jpx':
        case 'xbm':
        case 'wbmp':
        case 'png':
        case 'bmp':
        case 'tif':
        case 'tiff':
        case 'svg':
            $img = 'fas fa-picture-o';
            break;
        case 'passwd':
        case 'ftpquota':
        case 'sql':
        case 'js':
        case 'json':
        case 'sh':
        case 'config':
        case 'twig':
        case 'tpl':
        case 'md':
        case 'gitignore':
        case 'c':
        case 'cpp':
        case 'cs':
        case 'py':
        case 'map':
        case 'lock':
        case 'dtd':
            $img = 'fas fa-file-code-o';
            break;
        case 'txt':
        case 'ini':
        case 'conf':
        case 'log':
        case 'htaccess':
            $img = 'fas fa-file-text-o';
            break;
        case 'css':
        case 'less':
        case 'sass':
        case 'scss':
            $img = 'fas fa-css3';
            break;
        case 'zip':
        case 'rar':
        case 'gz':
        case 'tar':
        case '7z':
            $img = 'fas fa-file-archive-o';
            break;
        case 'php':
        case 'php4':
        case 'php5':
        case 'phps':
        case 'phtml':
            $img = 'fas fa-code';
            break;
        case 'htm':
        case 'html':
        case 'shtml':
        case 'xhtml':
            $img = 'fas fa-html5';
            break;
        case 'xml':
        case 'xsl':
            $img = 'fas fa-file-excel-o';
            break;
        case 'wav':
        case 'mp3':
        case 'mp2':
        case 'm4a':
        case 'aac':
        case 'ogg':
        case 'oga':
        case 'wma':
        case 'mka':
        case 'flac':
        case 'ac3':
        case 'tds':
            $img = 'fas fa-music';
            break;
        case 'm3u':
        case 'm3u8':
        case 'pls':
        case 'cue':
            $img = 'fas fa-headphones';
            break;
        case 'avi':
        case 'mpg':
        case 'mpeg':
        case 'mp4':
        case 'm4v':
        case 'flv':
        case 'f4v':
        case 'ogm':
        case 'ogv':
        case 'mov':
        case 'mkv':
        case '3gp':
        case 'asf':
        case 'wmv':
            $img = 'fas fa-file-video';
            break;
        case 'eml':
        case 'msg':
            $img = 'fas fa-envelope';
            break;
        case 'xls':
        case 'xlsx':
            $img = 'fas fa-file-excel';
            break;
        case 'csv':
            $img = 'fas fa-file-text';
            break;
        case 'bak':
            $img = 'fas fa-clipboard';
            break;
        case 'doc':
        case 'docx':
            $img = 'fas fa-file-word text-blue';
            break;
        case 'ppt':
        case 'pptx':
            $img = 'fas fa-file-powerpoint text-orange';
            break;
        case 'ttf':
        case 'ttc':
        case 'otf':
        case 'woff':
        case 'woff2':
        case 'eot':
        case 'fon':
            $img = 'fas fa-font';
            break;
        case 'pdf':
            $img = 'fas fa-file-pdf text-danger';
            break;
        case 'psd':
        case 'ai':
        case 'eps':
        case 'fla':
        case 'swf':
            $img = 'fas fa-file-image';
            break;
        case 'exe':
        case 'msi':
            $img = 'fas fa-file';
            break;
        case 'bat':
            $img = 'fas fa-terminal';
            break;
        default:
            $img = 'fas fa-info-circle';
    }

    return $img;
}

/**
 * Get image files extensions
 * @return array
 */
function fm_get_image_exts()
{
    return array('ico', 'gif', 'jpg', 'jpeg', 'jpc', 'jp2', 'jpx', 'xbm', 'wbmp', 'png', 'bmp', 'tif', 'tiff', 'psd');
}

/**
 * Get video files extensions
 * @return array
 */
function fm_get_video_exts()
{
    return array('webm', 'mp4', 'm4v', 'ogm', 'ogv', 'mov');
}

/**
 * Get audio files extensions
 * @return array
 */
function fm_get_audio_exts()
{
    return array('wav', 'mp3', 'ogg', 'm4a');
}

/**
 * Get text file extensions
 * @return array
 */
function fm_get_text_exts()
{
    return array(
        'txt', 'css', 'ini', 'conf', 'log', 'htaccess', 'passwd', 'ftpquota', 'sql', 'js', 'json', 'sh', 'config',
        'php', 'php4', 'php5', 'phps', 'phtml', 'htm', 'html', 'shtml', 'xhtml', 'xml', 'xsl', 'm3u', 'm3u8', 'pls', 'cue',
        'eml', 'msg', 'csv', 'bat', 'twig', 'tpl', 'md', 'gitignore', 'less', 'sass', 'scss', 'c', 'cpp', 'cs', 'py',
        'map', 'lock', 'dtd', 'svg',
    );
}

/**
 * Get mime types of text files
 * @return array
 */
function fm_get_text_mimes()
{
    return array(
        'application/xml',
        'application/javascript',
        'application/x-javascript',
        'image/svg+xml',
        'message/rfc822',
    );
}

/**
 * Get file names of text files w/o extensions
 * @return array
 */
function fm_get_text_names()
{
    return array(
        'license',
        'readme',
        'authors',
        'contributors',
        'changelog',
    );
}

/**
 * Class to work with zip files (using ZipArchive)
 */
class FM_Zipper
{
    private $zip;

    public function __construct()
    {
        $this->zip = new ZipArchive();
    }

    /**
     * Create archive with name $filename and files $files (RELATIVE PATHS!)
     * @param string $filename
     * @param array|string $files
     * @return bool
     */
    public function create($filename, $files)
    {
        $res = $this->zip->open($filename, ZipArchive::CREATE);
        if ($res !== true) {
            return false;
        }
        if (is_array($files)) {
            foreach ($files as $f) {
                if (!$this->addFileOrDir($f)) {
                    $this->zip->close();
                    return false;
                }
            }
            $this->zip->close();
            return true;
        } else {
            if ($this->addFileOrDir($files)) {
                $this->zip->close();
                return true;
            }
            return false;
        }
    }

    /**
     * Extract archive $filename to folder $path (RELATIVE OR ABSOLUTE PATHS)
     * @param string $filename
     * @param string $path
     * @return bool
     */
    public function unzip($filename, $path)
    {
        $res = $this->zip->open($filename);
        if ($res !== true) {
            return false;
        }
        if ($this->zip->extractTo($path)) {
            $this->zip->close();
            return true;
        }
        return false;
    }

    /**
     * Add file/folder to archive
     * @param string $filename
     * @return bool
     */
    private function addFileOrDir($filename)
    {
        if (is_file($filename)) {
            return $this->zip->addFile($filename);
        } elseif (is_dir($filename)) {
            return $this->addDir($filename);
        }
        return false;
    }

    /**
     * Add folder recursively
     * @param string $path
     * @return bool
     */
    private function addDir($path)
    {
        if (!$this->zip->addEmptyDir($path)) {
            return false;
        }
        $objects = scandir($path);
        if (is_array($objects)) {
            foreach ($objects as $file) {
                if ($file != '.' && $file != '..') {
                    if (is_dir($path . '/' . $file)) {
                        if (!$this->addDir($path . '/' . $file)) {
                            return false;
                        }
                    } elseif (is_file($path . '/' . $file)) {
                        if (!$this->zip->addFile($path . '/' . $file)) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }
        return false;
    }
}


/**
 * Scan directory and return tree view
 * @param string $directory
 * @param boolean $first_call
 */
function php_file_tree_dir($directory, $first_call = true)
{
    // Recursive function called by php_file_tree() to list directories/files

    $php_file_tree = "";
    // Get and sort directories/files
    if (function_exists("scandir")) $file = scandir($directory);
    natcasesort($file);
    // Make directories first
    $files = $dirs = array();
    foreach ($file as $this_file) {
        if (is_dir("$directory/$this_file")) {
            $dirs[] = $this_file;
        } else {
            $files[] = $this_file;
        }
    }
    $file = array_merge($dirs, $files);

    if (count($file) > 2) { // Use 2 instead of 0 to account for . and .. "directories"
        $php_file_tree = "<ul";
        if ($first_call) {
            $php_file_tree .= " class=\"pl-2 pt-2 php-file-tree\"";
            $first_call = false;
        }
        $php_file_tree .= ">";
        foreach ($file as $this_file) {
            if ($this_file != "." && $this_file != "..") {
                if (is_dir("$directory/$this_file")) {
                    // Directory
                    $php_file_tree .= "<li class=\"pft-directory\"><i class=\"fas fa-folder\"></i><a href=\"#\">" . htmlspecialchars($this_file) . "</a>";
                    $php_file_tree .= php_file_tree_dir("$directory/$this_file", false);
                    $php_file_tree .= "</li>";
                } else {
                    // File
                    $ext = fm_get_file_icon_class($this_file);
                    $path = str_replace($_SERVER['DOCUMENT_ROOT'], "", $directory);
                    $link = "?p=" . "$path" . "&view=" . urlencode($this_file);
                    $php_file_tree .= "<li class=\"pft-file\"><a href=\"$link\"> <i class=\"$ext\"></i>" . htmlspecialchars($this_file) . "</a></li>";
                }
            }
        }
        $php_file_tree .= "</ul>";
    }
    return $php_file_tree;
}

function mapFolder($directory)
{
    if (function_exists("scandir")) $file = scandir($directory);
    natcasesort($file);
    // Make directories first
    $files = $dirs = array();
    for ($index = 0; $index < count($file); $index++):
        if ($file[$index] == '.' or $file[$index] == '..') {
            continue;
        }
        if (is_dir("$directory/$file[$index]")) {
            $dirs[] = $file[$index];
            //print_r(scandir("$directory/$this_file"));
            //$files[] = scandir("$directory/$this_file");
            $file = array_merge_recursive($file, scandir("$directory/$file[$index]"));
            //print_r($file);

        } else {
            $files[] = $file[$index];
        }
    endfor;

    return array_merge($dirs, $files);
}

function mapearPastas($path, $data = [])
{
    if (function_exists("scandir")) $dirs = scandir($path);
    for ($index = 0; $index < count($dirs); $index++):
        if ($dirs[$index] == '.' or $dirs[$index] == '..') {
            continue;
        }
        if (is_dir("$path/$dirs[$index]")):
            $data[$index]['name'] = $dirs[$index];
            $data[$index]['type'] = 'folder';
            $data[$index]['path'] = "$path/$dirs[$index]";
            $data[$index]['childrens'] = mapearPastas("$path/$dirs[$index]");
        else:
            $data[$index]['name'] = $dirs[$index];
            $data[$index]['type'] = 'file';
            $data[$index]['path'] = "$path/$dirs[$index]";
        endif;
    endfor;

    return $data;
}


/**
 * Get nice filesize
 * @param int $size
 * @return string
 */
function fm_get_filesize($size)
{
    if ($size < 1000) {
        return sprintf('%s B', $size);
    } elseif (($size / 1024) < 1000) {
        return sprintf('%s KiB', round(($size / 1024), 2));
    } elseif (($size / 1024 / 1024) < 1000) {
        return sprintf('%s MiB', round(($size / 1024 / 1024), 2));
    } elseif (($size / 1024 / 1024 / 1024) < 1000) {
        return sprintf('%s GiB', round(($size / 1024 / 1024 / 1024), 2));
    } else {
        return sprintf('%s TiB', round(($size / 1024 / 1024 / 1024 / 1024), 2));
    }
}

/**
 * This function scans the files folder recursively, and builds a large array
 * @param string $dir
 * @return json
 */
function scan($dir)
{
    $files = array();
    $_dir = $dir;
    //$dir = GENERAL_PATH.'/'.$dir;
    // Is there actually such a folder/file?
    if (file_exists($dir)) {
        foreach (scandir($dir) as $f) {
            if (!$f || $f[0] == '.') {
                continue; // Ignore hidden files
            }

            if (is_dir($dir . '/' . $f)) {
                // The path is a folder
                $files[] = array(
                    "name" => $f,
                    "type" => "folder",
                    "path" => $_dir . '/' . $f,
                    "items" => scan($_dir . '/' . $f), // Recursively get the contents of the folder
                );
            } else {
                // It is a file
                $files[] = array(
                    "name" => $f,
                    "type" => "file",
                    "path" => $_dir,
                    "size" => filesize($dir . '/' . $f) // Gets the size of this file
                );
            }
        }
    }
    return $files;
}

function verificarItem($path)
{
    return is_dir($path) or file_exists($path);
}


/*
 * Get MIME of file Extencion
 *  */

function fm_get_mime($path)
{
    // get extension
    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
    switch ($ext):
        case 'doc':
        case 'dot':
            return 'application/msword';
        case 'docx':
            return 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        case 'dotx':
            return 'application/vnd.openxmlformats-officedocument.wordprocessingml.template';
        case 'docm':
            return 'application/vnd.ms-word.document.macroEnabled.12';
        case 'dotm':
            return 'application/vnd.ms-word.template.macroEnabled.12';
        case 'xls':
        case 'xlt':
        case 'xla':
            return 'application/vnd.ms-excel';
        case 'xlsx':
            return 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        case 'xltx':
            return 'application/vnd.openxmlformats-officedocument.spreadsheetml.template';
        case 'xlsm':
            return 'application/vnd.ms-excel.sheet.macroEnabled.12';
        case 'pdf':
            return 'application/pdf';
        case 'txt':
            return 'text/plain';
        case 'zip':
            return 'application/zip';
        case 'bmp':
            return 'image/bmp';
        case 'gif':
            return 'image/gif';
        case 'jpe':
        case 'jpeg':
        case 'jpg':
            return 'image/jpeg';
        case 'jfif':
            return 'image/pipeg';
        case 'svg':
            return 'image/svg+xml';
        case 'ico':
            return 'image/x-icon';
        case '':
            return '';
        case 'ppt':
        case 'pot':
        case 'pps':
        case 'ppa':
            return 'application/mspowerpoint';
        case 'pptx':
            return 'application/vnd.openxmlformats-officedocument.presentationml.presentation';
        case 'potx':
            return 'application/vnd.openxmlformats-officedocument.presentationml.template';
        case 'ppsx':
            return 'application/vnd.openxmlformats-officedocument.presentationml.slideshow';
        case 'ppam':
            return 'application/vnd.ms-powerpoint.addin.macroEnabled.12';
        case 'pptm':
            return 'application/vnd.ms-powerpoint.presentation.macroEnabled.12';
        case 'ppsm':
            return 'application/vnd.ms-powerpoint.slideshow.macroEnabled.12';
        case 'potm':
            return 'application/vnd.ms-powerpoint.template.macroEnabled.12';
    endswitch;
}

function fm_get_extension($path)
{
    $ext = strtolower(pathinfo($path, PATHINFO_EXTENSION));
}

function copiarMover($dataInput)
{
 
 $dataReturn['message'] = null;
 $dataReturn['status'] = false;
// from
 $copy = $dataInput['target'];
 $copy = fm_clean_path($copy);
 // empty path
 if ($copy == '') {
     $dataReturn['message'] = 'Fonte do caminho não foi definido';
 }
 // abs path from
 $from = $copy;
 // abs path to
 $dest = dataInput['destino'];

 $dest .= '/' . basename($from);
 // move?
 $move = isset($dataInput['move']);
 
 // copy/move
 if ($from != $dest) {
     $msg_from = trim('/' . basename($from), '/');
     if ($move) {
         $rename = fm_rename($from, $dest);
         if ($rename) {
             $dataReturn['message'] = sprintf('Transferido de <b>%s</b> para <b>%s</b>', fm_enc($copy), fm_enc($msg_from));
             $dataReturn['status'] = true;
         } elseif ($rename === null) {
            $dataReturn['message'] = 'Ficheiro ou pasta com este caminha já existe';
             $dataReturn['status'] = true;
         } else {
            $dataReturn['message'] = sprintf('Erro ao tentar transferir de <b>%s</b> para <b>%s</b>', fm_enc($copy), fm_enc($msg_from));
            $dataReturn['status'] = false;
         }
     } else {
         if (fm_rcopy($from, $dest)) {
            $dataReturn['message'] = sprintf('Copiado de <b>%s</b> para <b>%s</b>', fm_enc($copy), fm_enc($msg_from));
            $dataReturn['status'] = true;
         } else {
            $dataReturn['message'] = sprintf('Erro ao tentar copiar de <b>%s</b> para <b>%s</b>', fm_enc($copy), fm_enc($msg_from));
            $dataReturn['status'] = false;

         }
     }
 } else {
     $dataReturn['message'] ='Caminhos não devem ser iguais';
     $dataReturn['status'] = false;
 }
 return $dataReturn;
}


/**
 * Encode html entities
 * @param string $text
 * @return string
 */
 function fm_enc($text)
 {
     return htmlspecialchars($text, ENT_QUOTES, 'UTF-8');
 }

 
/**
 * Safely rename
 * @param string $old
 * @param string $new
 * @return bool|null
 */
function fm_rename($old, $new)
{
    return (!file_exists($new) && file_exists($old)) ? rename($old, $new) : null;
}


/**
 * Clean path
 * @param string $path
 * @return string
 */
function fm_clean_path($path)
{
    $path = trim($path);
    $path = trim($path, '\\/');
    $path = str_replace(array('../', '..\\'), '', $path);
    if ($path == '..') {
        $path = '';
    }
    return str_replace('\\', '/', $path);
}


/**
 * Copy file or folder (recursively).
 * @param string $path
 * @param string $dest
 * @param bool $upd Update files
 * @param bool $force Create folder with same names instead file
 * @return bool
 */
function fm_rcopy($path, $dest, $upd = true, $force = true)
{
    if (is_dir($path)) {
        if (!fm_mkdir($dest, $force)) {
            return false;
        }
        $objects = scandir($path);
        $ok = true;
        if (is_array($objects)) {
            foreach ($objects as $file) {
                if ($file != '.' && $file != '..') {
                    if (!fm_rcopy($path . '/' . $file, $dest . '/' . $file)) {
                        $ok = false;
                    }
                }
            }
        }
        return $ok;
    } elseif (is_file($path)) {
        return fm_copy($path, $dest, $upd);
    }
    return false;
}

/**
 * Safely create folder
 * @param string $dir
 * @param bool $force
 * @return bool
 */
function fm_mkdir($dir, $force)
{
    if (file_exists($dir)) {
        if (is_dir($dir)) {
            return EXIST_ITEM;
        } elseif (!$force) {
            return IS_NOT_DIR;
        }
        unlink($dir);
    }
    return mkdir($dir, 0777, true) ? FOLDER_CREATE_SUCCESS : FOLDER_CREATE_ERROR;
}

/**
 * Safely copy file
 * @param string $f1
 * @param string $f2
 * @param bool $upd
 * @return bool
 */
function fm_copy($f1, $f2, $upd)
{
    $time1 = filemtime($f1);
    if (file_exists($f2)) {
        $time2 = filemtime($f2);
        if ($time2 >= $time1 && $upd) {
            return false;
        }
    }
    $ok = copy($f1, $f2);
    if ($ok) {
        touch($f2, $time1);
    }
    return $ok;
}

/**
 * Get mime type
 * @param string $file_path
 * @return mixed|string
 */
function fm_get_mime_type($file_path)
{
    if (function_exists('finfo_open')) {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $file_path);
        finfo_close($finfo);
        return $mime;
    } elseif (function_exists('mime_content_type')) {
        return mime_content_type($file_path);
    } elseif (!stristr(ini_get('disable_functions'), 'shell_exec')) {
        $file = escapeshellarg($file_path);
        $mime = shell_exec('file -bi ' . $file);
        return $mime;
    } else {
        return '--';
    }
}


function fm_convert_win($filename)
{
    if (FM_IS_WIN && function_exists('iconv')) {
        $filename = iconv(FM_ICONV_INPUT_ENC, 'UTF-8//IGNORE', $filename);
    }
    return $filename;
}


/**
 * Delete  file or folder (recursively)
 * @param string $path
 * @return bool
 */
function fm_rdelete($path)
{
    if (is_link($path)) {
        return unlink($path);
    } elseif (is_dir($path)) {
        $objects = scandir($path);
        $ok = true;
        if (is_array($objects)) {
            foreach ($objects as $file) {
                if ($file != '.' && $file != '..') {
                    if (!fm_rdelete($path . '/' . $file)) {
                        $ok = false;
                    }
                }
            }
        }
        return ($ok) ? rmdir($path) : false;
    } elseif (is_file($path)) {
        return unlink($path);
    }
    return false;
}