<?php


namespace App\Controllers;


use Config\Services;
use phpDocumentor\Reflection\Types\This;

class Inicio extends BaseController
{
    public $dataView = [];
    protected $session;

    /**
     * Inicio constructor.
     */
    public function __construct()
    {

        $this->session = Services::session();
        $funcionalidades = new \App\Models\FuncionalidadeModel();

        $this->dataView['currectModulo'] = 'Inicio';
        $this->dataView['currectSubModulo'] = 'Inicio';

        $this->dataView['classPage'] = 'sidebar-mini';
        $this->dataView['dataUser'] = $this->session->isLoggedIn ? $this->session->dadosUtilizador : null;
        $gestaoAcesso = new \App\Models\GestaoAcessoModel();
        $pastasAcessos = new \App\Models\ArmazenamentoModel();
        if ($this->session->isLoggedIn) :
            $this->dataView['acessosUtilizador'] = $gestaoAcesso->where('utilizador', $this->dataView['dataUser']['identificador'])->findAll();
            $perfil = new \App\Models\PerfilUtilizadorModel();
            $detalhePerfil = $perfil->find($this->dataView['dataUser']['perfil']);
            $this->dataView['funcionalidades'] = $funcionalidades->where(['estado' => 1, 'nivelAcesso >=' => $detalhePerfil->nivelAcesso])->findAll();
            $this->dataView['pastasUtilizador'] = $pastasAcessos->whereIn('grupoAcesso', $this->session->dadosUtilizador['codigosAcessos'])->findAll();
        endif;
    }

    public function index()
    {
        if (!$this->session->isLoggedIn) :
            return redirect()->to('/utilizador/autenticacao');
        else :
            $this->dataView['namePage'] = '';
            $this->dataView['submoduloName'] = '';
            //$this->dataView['armazenamentos'] = scandir(WRITEPATH.'/uploads/armazenamento');
            $mainPath = GENERAL_PATH;
            $objects = is_readable($mainPath) ? scandir($mainPath) : array();
            $folders = array();
            $files = array();
            if (is_array($objects)) {
                foreach ($objects as $file) {
                    $new_path = $mainPath . '/' . $file;
                    if (is_file($new_path)) {
                        clearstatcache();
                        $folders[] = array('nome' => $file, 'size' => filesize($new_path), 'type' => filetype($new_path));
                    } elseif (is_dir($new_path) && $file != '.' && $file != '..') {
                        $folders[] = array('nome' => $file, 'size' => (count(scandir($new_path)) - 2), 'type' => filetype($new_path));
                    }
                }
            }
            $this->dataView['armazenamentos'] = $folders;
            return view('pageInicio', $this->dataView);
        endif;
    }

    public function iniciarSessao()
    {
        if (!$this->session->isLoggedIn) :
            $this->dataView['classPage'] = 'login-page';
            return view('pageAutenticacao', $this->dataView);
        else :
            return redirect()->to('/inicio');
        endif;
    }

    public function terminarSessao()
    {
        $this->session->destroy();
        return redirect()->to('/utilizador/autenticacao');
    }

    public function autenticarUtilizador()
    {
        $dataCredencias['email'] = $this->request->getPost('email', FILTER_SANITIZE_EMAIL);
        $dataCredencias['password'] = $this->request->getPost('psw', FILTER_SANITIZE_STRING);
        $modelUser = new \App\Models\UtilizadorModel();
        $dataValidate = $modelUser->autenticarAcesso($dataCredencias);


        if (is_array($dataValidate)) :
            $this->session->set('isLoggedIn', true);
            $this->session->set('dadosUtilizador', $dataValidate);
            return redirect()->to('/inicio');
        else :
            return redirect()->to('/utilizador/autenticacao');
        endif;
    }

    public function pageRegistoUtilizador()
    {
        if ($this->session->isLoggedIn and $this->session->dadosUtilizador['perfil'] === 'administrador') :
            $this->dataView['classPage'] = 'sidebar-mini';
            $this->dataView['currectModulo'] = 'Gestão de Utilizadores';
            $this->dataView['namePage'] = 'Gestão de Utilizadores';
            $this->dataView['submoduloName'] = 'Registo de Utilizador';
            $this->dataView['currectSubModulo'] = 'FCN20208641';
            $icones = new \App\Models\IconeModel();
            $this->dataView['icones'] = $icones->findAll();
            $accessGroup = new \App\Models\GrupoAcessoModel();
            $permitions = new \App\Models\PermissaoModel();
            $this->dataView['accessGroups'] = $accessGroup->findAll();
            $this->dataView['permitions'] = $permitions->findAll();

            return view('pageRegistoUtilizador', $this->dataView);
        else :
            return view('errors/html/error_404');
        endif;
    }

    public function inserirUtilizador()
    {
        $dataReturn['status'] = false;
        if ($this->session->isLoggedIn) :
            $modelUser = new \App\Models\UtilizadorModel();
            $modelGA = new \App\Models\GestaoAcessoModel();
            $gestaoAcesso = new \App\Entities\Gestaoacesso();
            $dataInput = $this->request->getPost(null);
            $idutilizador = $modelUser->generate_key();
            $user = new \App\Entities\Utilizador(array(
                'idutilizador' => $idutilizador->novoUser,
                'nome' => $dataInput['nome'],
                'email' => $dataInput['email'],
                'perfil' => $dataInput['perfil']

            ));
            $user->setPassword('12345syfile');
            $modelUser->insert($user);
            if ($this->db->affectedRows() > 0) {
                $ctrl = true;
                foreach ($dataInput['grupoAcesso'] as $grupo) {
                    $dataInput['permissoes'][$grupo]['grupoAcesso'] = $grupo;
                    $dataInput['permissoes'][$grupo]['utilizador'] = $idutilizador->novoUser;
                    $gestaoAcesso->fill($dataInput['permissoes'][$grupo]);
                    $modelGA->insert($gestaoAcesso);
                    if ($this->db->affectedRows() <= 0) {
                        $ctrl = false;
                    }
                }
                $dataReturn['status'] = $ctrl;
                if (!$ctrl) {
                    $dataReturn['message'] = "Error Number: INCIUR" . __LINE__ . " - Não foi possivel efectuar registo Contactar o administrador";
                }
            } else {
                $dataReturn['message'] = "Error Number: INCIUR" . __LINE__ . " - Não foi possivel efectuar registo Contactar o administrador";
            }
        else :
            $dataReturn['message'] = sprintf("Error %s: Não possui permissões executar a acção solicitada" . str_pad(__LINE__, 3, "0", STR_PAD_LEFT));
        endif;
        echo json_encode($dataReturn);
    }


    public function pageListarUtilizadores()
    {
        if ($this->session->isLoggedIn) :
            $this->dataView['classPage'] = 'sidebar-mini';
            $this->dataView['currectModulo'] = 'Gestão de Utilizadores';
            $this->dataView['namePage'] = 'Utilizadores';
            $this->dataView['submoduloName'] = 'Utilizadores';
            $this->dataView['currectSubModulo'] = 'FCN20208462';
            $users = new \App\Models\UtilizadorModel();
            $gestaoAcessos = new \App\Models\GestaoAcessoModel();

            $this->dataView['utilizadores'] = $users->paginate(6, 'pagina');
            $this->dataView['gestaoAcesso'] = $gestaoAcessos;
            $this->dataView['pager'] = $users->pager;
            return view('pageListarUtilizadores', $this->dataView);
        else :
            return view('errors/html/error_404');
        endif;
    }

    public function testePDF()
    {
        $this->dataView['classPage'] = 'sidebar-mini';
        $this->dataView['currectModulo'] = 'Gestão de Utilizadores';
        $this->dataView['namePage'] = 'Utilizadores';
        $this->dataView['submoduloName'] = 'Utilizadores';
        $this->dataView['currectSubModulo'] = 'FCN20208462';
        $users = new \App\Models\UtilizadorModel();
        $gestaoAcessos = new \App\Models\GestaoAcessoModel();

        $this->dataView['utilizadores'] = $users->paginate(6, 'pagina');
        $this->dataView['gestaoAcesso'] = $gestaoAcessos;
        $this->dataView['pager'] = $users->pager;

        $dompdf = new \Dompdf\Dompdf();
        $dompdf->loadHtml(view('pageListarUtilizadores', $this->dataView));
        $dompdf->setPaper('A4', 'landscape');
        $dompdf->render();
        $dompdf->stream('teste.pdf', array("Attachment" => false));
    }

    public function pageFolderDeteail($folderName, $pathFolder)
    {
        if ($this->session->isLoggedIn) :

            $this->dataView['folderName'] = base64_decode($folderName);
            $this->dataView['pathFolder'] = base64_decode($pathFolder);
            $modelArmazenamento = new \App\Models\ArmazenamentoModel();
            $this->dataView['armazenamento'] = $modelArmazenamento->where('caminho', str_replace(GENERAL_PATH . '/', '', $this->dataView['pathFolder']))->first();
            $this->dataView['submoduloName'] = $this->dataView['armazenamento']->caminho;
            //$this->dataView['namePage'] = $this->dataView['pathFolder'];
            $this->dataView['acessoPasta'] = !empty($this->dataView['armazenamento']->grupoAcesso) ? $this->dataView['armazenamento']->grupoAcesso : null;
            return view('pageFolderDetail', $this->dataView);
        else :
            return view('errors/html/error_404');
        endif;
    }

    public function pageVisualizarFile($fileName, $pathFile)
    {
        if ($this->session->isLoggedIn) :
            $fileName = base64_decode($fileName);
            $pathFile = base64_decode($pathFile);
            if (is_file("$pathFile/$fileName")) :
                $tipo = $this->request->getGet('type', FILTER_SANITIZE_STRING);
                switch ($tipo) {
                    case 'v':
                        header('Content-Disposition: inline; filename="' . $fileName . '"');
                        break;
                    default:
                        header('Content-Disposition: attachment; filename="' . $fileName . '"');
                }
                //$this->response->setHeader('filename',"$fileName");
                $this->response->setContentType(fm_get_mime_type("$pathFile/$fileName"));
                readfile($pathFile . '/' . $fileName);
            endif;
        else :
            return view('errors/html/error_404');
        endif;
    }

    public function downloadFiles($fileName, $pathFile)
    {
        $pathFile = base64_decode($pathFile);
        $fileName = base64_decode($fileName);

        if ($fileName != '' && is_file($pathFile . '/' . $fileName)) {
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . basename($pathFile . '/' . $fileName) . '"');
            header('Content-Transfer-Encoding: binary');
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($pathFile . '/' . $fileName));
            readfile($pathFile . '/' . $fileName);
            exit;
        }
    }

    public function criarPasta()
    {
        $dataReturn['status'] = false;
        if ($this->session->isLoggedIn) :
            $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
            $dataInput['path'] = explode('||', $dataInput['path']);

            $idOrigem = null;
            if (count($dataInput['path']) > 1) :
                $path = fm_clean_path($dataInput['path'][1]);
                $idOrigem = $dataInput['path'][0];
            else :
                $path = fm_clean_path($dataInput['path'][0]);
            endif;

            switch (fm_mkdir(GENERAL_PATH . '/' . $path . '/' . $dataInput['folder-name'], true)):
                case EXIST_ITEM:
                    $dataReturn['message'] = sprintf('Error %s: Pasta <b>%s</b> já existe', str_pad(__LINE__, 3, "0", STR_PAD_LEFT), fm_enc($dataInput['folder-name']));
                    break;
                case IS_NOT_DIR:
                case FOLDER_CREATE_ERROR:
                    $dataReturn['message'] = sprintf('Error %s: Não possivel criar a pasta <b>%s</b>', str_pad(__LINE__, 3, "0", STR_PAD_LEFT), fm_enc($dataInput['folder-name']));
                    break;
                case FOLDER_CREATE_SUCCESS:

                    $novoArmazenamento = new \App\Models\ArmazenamentoModel();

                    $idArmazenamento = $novoArmazenamento->generate_key();
                    $armazenamentoEntidade = new \App\Entities\Armazenamento([
                        'idarmazenamento' => $idArmazenamento,
                        'origem' => $idOrigem,
                        'descricao' => $dataInput['folder-name'],
                        'caminho' => $path . '/' . $dataInput['folder-name'],
                        'grupoAcesso' => $dataInput['grupoAcesso'],
                        'color' => $dataInput['colorFolder']
                    ]);
                    $dataReturn['id'] = $idArmazenamento;

                    $novoArmazenamento->insert($armazenamentoEntidade);

                    if ($this->db->affectedRows() > 0) :
                        $dataReturn['message'] = sprintf('Pasta %s criada com sucesso', $dataInput['folder-name']);
                        $dataReturn['status'] = true;
                    else :
                        $dataReturn['message'] = sprintf('Error %s: Não possivel criar a pasta %s', str_pad(__LINE__, 3, "0", STR_PAD_LEFT), fm_enc($dataInput['folder-name']));
                    endif;

                    break;
            endswitch;
        else :
            $dataReturn['message'] = sprintf("Error %s: Não possui permissões executar a acção solicitada" . str_pad(__LINE__, 3, "0", STR_PAD_LEFT));
        endif;
        echo json_encode($dataReturn);
    }

    function uploadFicheiros()
    {
        $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
        $dataInput['path'] = explode('||', $dataInput['path']);
        $idarmazenamento = $dataInput['path'][0];
        $path = fm_clean_path($dataInput['path'][1]);
        $ficheiroModel = new \App\Models\FicheiroModel();
        $ficheiroEntidade = new \App\Entities\Ficheiro();
        $dataReturn['totalErros'] = 0;
        $dataReturn['totalSucesso'] = 0;
        $dataReturn['total'] = 0;
        if ($imagefile = $this->request->getFiles()) {
            $ctrl = true;
            foreach ($imagefile['file'] as $ficheiro) {
                $dataReturn['total'] += 1;
                if ($ficheiro->isValid() && !$ficheiro->hasMoved()) {
                    $newName = $ficheiro->getRandomName();
                    $ficheiro->move(GENERAL_PATH . '/' . $path, $newName);
                    $idFicheiro = $ficheiroModel->generate_key();
                    $ficheiroEntidade->fill([
                        'idficheiro' => $idFicheiro,
                        'descricao' => $newName,
                        'caminho' => $path . '/' . $newName,
                        'armazenamento' => $idarmazenamento
                    ]);
                    $ficheiroModel->insert($ficheiroEntidade);
                    if ($this->db->affectedRows() > 0) {
                        $dataReturn['totalSucesso'] += 1;
                    } else {
                        $dataReturn['totalErros'] += 1;
                    }
                }
            }
        }
        if ($dataReturn['totalSucesso'] > 0 and $dataReturn['totalErros'] == 0) {
            # code...
            $dataReturn['status'] = true;
            $dataReturn['message'] = sprintf("%s de %s de ficheiros foram carregados com sucesso", $dataReturn['totalSucesso'], $dataReturn['total']);
        } else {
            $dataReturn['status'] = false;
            $dataReturn['message'] = sprintf("%s de %s de ficheiros não foram carregados com sucesso. Erro ULF-%s", $dataReturn['totalErros'], $dataReturn['total'], str_pad(__LINE__, 3, '0', STR_PAD_LEFT));
        }
        echo json_encode($dataReturn);
    }

    public function editarFicheiro()
    {
        $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
        $newName = fm_clean_path($dataInput['path'] . '/' . $dataInput['newName']);
        $oldName = fm_clean_path($dataInput['path'] . '/' . $dataInput['oldNameFile']);

        $folder = new \App\Models\ArmazenamentoModel();
        $file = new \App\Models\FicheiroModel();
        $dataReturn['status'] = false;

        if (!file_exists(GENERAL_PATH . '/' . $oldName)) {
            $oldName = str_replace($dataInput['oldNameFile'] . '/', '', $oldName);
            $newName = str_replace($dataInput['oldNameFile'] . '/', '', $newName);
        }
        $dataReturn['inp'] = [GENERAL_PATH . '/' . $newName, GENERAL_PATH . '/' . $oldName];
        if (fm_rename(GENERAL_PATH . '/' . $oldName, GENERAL_PATH . '/' . $newName)) :
            if (strlen($dataInput['idItem']) > 1) {

                if (strpos($dataInput['idItem'], 'FLD') === 0) {
                    $targetFolder = $folder->find($dataInput['idItem']);
                    $novoCaminho = str_replace($dataInput['oldNameFile'], $dataInput['newName'], $targetFolder->caminho);
                    $folder->save([
                        'idarmazenamento' => $dataInput['idItem'],
                        'caminho' => $novoCaminho,
                        'descricao' => $dataInput['newName']
                    ]);
                } else {
                    $targetFile = $file->find($dataInput['idItem']);
                    $novoCaminho = str_replace($dataInput['oldNameFile'], $dataInput['newName'], $targetFile->caminho);
                    $fileEntidade = new \App\Entities\Ficheiro([
                        'idficheiro' => $dataInput['idItem'],
                        'caminho' => $novoCaminho,
                        'descricao' => $dataInput['newName']
                    ]);
                    $file->save($fileEntidade);
                }

                if ($this->db->affectedRows() > 0) :
                    $dataReturn['status'] = true;
                    $dataReturn['message'] = sprintf('Renomeado de %s para %s', fm_enc($dataInput['oldNameFile']), fm_enc($dataInput['newName']));
                else :
                    $dataReturn['status'] = false;
                    $dataReturn['message'] = sprintf('Erro ao tentar salvar a renomeação de %s para %s', fm_enc($dataInput['oldNameFile']), fm_enc($dataInput['newName']));
                endif;
            } else {
                $dataReturn['status'] = true;
                $dataReturn['message'] = sprintf('Renomeado de %s para %s', fm_enc($dataInput['oldNameFile']), fm_enc($dataInput['newName']));
            }
        else :
            $dataReturn['status'] = false;

            $dataReturn['message'] = sprintf('Erro ao tentar renomear de %s para %s', fm_enc($dataInput['oldNameFile']), fm_enc($dataInput['newName']));
        endif;
        echo json_encode($dataReturn);
    }

    public function moverCopiarFicheiro()
    {
        $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
        $dataReturn['status'] = false;

        $dataInput['pathDestino'] = explode('||', $dataInput['pathDestino']);
        $pathDestino = fm_clean_path($dataInput['type'] === 'file' ? $dataInput['pathDestino'][1] . '/' . $dataInput['nameFile'] : $dataInput['pathDestino'][1]);
        $idFolderDestino = $dataInput['pathDestino'][0];
        $pathOrigem = fm_clean_path($dataInput['type'] === 'file' ? $dataInput['path'] . '/' . $dataInput['nameFile'] : $dataInput['path']);
        $dataReturn['input'] = [$pathDestino, fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, fm_clean_path(GENERAL_PATH) . '/' . $pathDestino];
        $folderModel = new \App\Models\ArmazenamentoModel();
        $fileModel = new \App\Models\FicheiroModel();
        if (isset($dataInput['isCopy'])) :
            #copiar ficheiros e pastas
            if (fm_rcopy(fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, fm_clean_path(GENERAL_PATH) . '/' . $pathDestino)) {
                if (strlen($dataInput['idItem']) > 1) :
                    switch ($dataInput['type']) {
                        case 'folder':
                            # Copy/Move Folders
                            $novoIdFolder = $folderModel->generate_key();
                            $folderTarget = $folderModel->find($dataInput['idItem']);
                            $folderModel->insert([
                                'idarmazenamento' => $novoIdFolder,
                                'origem' => $idFolderDestino,
                                'descricao' => $dataInput['nameFile'],
                                'caminho' => $pathDestino,
                                'grupoAcesso' => $folderTarget->grupoAcesso
                            ]);
                            break;
                        case 'file':
                            # copy/move files
                            $fileTarget = $fileModel->find($dataInput['idItem']);
                            $novoIDFile = $fileModel->generate_key();
                            $fileModel->insert([
                                'idficheiro' => $novoIDFile,
                                'descricao' => $dataInput['nameFile'],
                                'caminho' => $pathDestino,
                                'resumo' => $fileTarget->destino,
                                'armazenamento' => $idFolderDestino
                            ]);
                            break;
                        default:
                            # Opcao Invalida
                            $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                    }
                    if ($this->db->affectedRows() > 0) :
                        $dataReturn['status'] = true;
                        $dataReturn['message'] = sprintf('Copiado de %s para %s', fm_enc($pathOrigem), fm_enc($pathDestino));
                    else :
                        $dataReturn['message'] = sprintf('Erro %s: Não foi possivek salvar Alterações Solicitads', __LINE__);
                    endif;
                endif;
            } else {
                $dataReturn['message'] = sprintf('ERROR %s: Erro ao tentar copiar de %s para %s', __LINE__, fm_enc($pathOrigem), fm_enc($pathDestino));
            }
        else :
            #mover Ficheiros e pastas
            $rename = fm_rename(fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, fm_clean_path(GENERAL_PATH) . '/' . $pathDestino);
            if ($rename) {
                if (strlen($dataInput['idItem']) > 1) :
                    switch ($dataInput['type']) {
                        case 'folder':
                            # Copy/Move Folders
                            $folderTarget = $folderModel->find($dataInput['idItem']);
                            $folderModel->insert([
                                'idarmazenamento' => $folderTarget->idarmazenamento,
                                'origem' => $idFolderDestino,
                                'caminho' => $pathDestino
                            ]);
                            break;
                        case 'file':
                            # copy/move files
                            $fileTarget = $fileModel->find($dataInput['idItem']);
                            $fileModel->save([
                                'idficheiro' => $fileTarget->idficheiro,
                                'caminho' => $pathDestino,
                                'armazenamento' => $idFolderDestino
                            ]);
                            break;
                        default:
                            # Opcao Invalida
                            $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                    }
                    if ($this->db->affectedRows() > 0) :
                        $dataReturn['status'] = true;
                        $dataReturn['message'] = sprintf('Transferido de %s para %s', fm_enc($pathOrigem), fm_enc($pathDestino));
                    else :
                        $dataReturn['message'] = sprintf('Erro %s: Não foi possiveL salvar Alterações Solicitads', __LINE__);
                    endif;
                endif;
            } elseif ($rename === null) {
                $dataReturn['message'] = 'Ficheiro ou pasta com este caminha já existe';
            } else {
                $dataReturn['message'] = sprintf('Erro ao tentar transferir de %s para %s', fm_enc($pathOrigem), fm_enc($pathDestino));
            }

        endif;
        echo json_encode($dataReturn);
    }

    public function deleteFilesFolders()
    {
        $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
        $reciclagemPath = fm_clean_path(GENERAL_PATH . '/reciclagem');
        $dataReturn['status'] = false;
        $folderModel = new \App\Models\ArmazenamentoModel();
        $fileModel = new \App\Models\FicheiroModel();
        $pathOrigem = fm_clean_path($dataInput['type'] === 'file' ? $dataInput['pathItem'] . '/' . $dataInput['nameFile'] : $dataInput['pathItem']);
        $dataReturn['teste'] = [fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, $reciclagemPath . '/' . $dataInput['nameFile']];
        switch ($dataInput['typeOp']) {
            case 'reciclagem':
                # Eliminar de forma não permanente
                $rename = fm_rename(fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, $reciclagemPath . '/' . $dataInput['nameFile']);
                if ($rename) {
                    if (strlen($dataInput['idItem']) > 1) :
                        switch ($dataInput['type']) {
                            case 'folder':
                                # eliminar pastas temporariamente (mandar para reciclagem)...
                                $folderTarget = $folderModel->find($dataInput['idItem']);
                                $folderModel->save([
                                    'idarmazenamento' => $folderTarget->idarmazenamento,
                                    'estado' => 0
                                ]);
                                break;
                            case 'file':
                                $fileTarget = $fileModel->find($dataInput['idItem']);
                                $fileModel->save([
                                    'idficheiro' => $fileTarget->idficheiro,
                                    'estado' => 0
                                ]);
                                break;

                            default:
                                # code...
                                $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                                break;
                        }
                        if ($this->db->affectedRows() > 0) :
                            $dataReturn['status'] = true;
                            $dataReturn['message'] = sprintf('Ficheiro %s enviado para Reciclagem', $dataInput['nameFile']);
                        else :
                            $dataReturn['message'] = sprintf('Erro %s: Não foi possiveL salvar Alterações Solicitadas', __LINE__);
                        endif;
                    else :
                        $dataReturn['status'] = true;
                        $dataReturn['message'] = sprintf('Ficheiro %s enviado para Reciclagem', $dataInput['nameFile']);
                    endif;
                } elseif (is_null($rename)) {
                    $dataReturn['message'] = sprintf('ERROR DFF - %S: Ficheiro ou pasta com este caminha já existe', __LINE__);
                } else {
                    $dataReturn['message'] = sprintf('ERROR DFF-%s: Erro ao tentar transferir de %s para Reciclagem', __LINE__, $dataInput['nameFile']);
                }
                break;
            case 'permanente':
                if (fm_rdelete(fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem)) {
                    if (strlen($dataInput['idItem']) > 1) :
                        $ctrl = false;
                        switch ($dataInput['type']) {
                            case 'folder':
                                # code...
                                # eliminar pastas temporariamente (mandar para reciclagem)...
                                $fileModel->where('armazenamento', $dataInput['idItem'])->delete();
                                $ctrl = $this->db->affectedRows() > 0 ? $folderModel->delete($dataInput['idItem']) : null;
                                break;
                            case 'file':
                                $fileModel->delete($dataInput['idItem']);
                                $ctrl = $this->db->affectedRows() > 0;
                                break;
                            default:
                                # code...
                                $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                                break;
                        }
                        if ($ctrl) {
                            $dataReturn['status'] = true;
                            $dataReturn['message'] = sprintf('Ficheiro %s eliminado permanentemente', $dataInput['nameFile']);
                        } else {
                            $dataReturn['message'] = sprintf('Erro %s: Não foi possiveL salvar Alterações Solicitadas', __LINE__);
                        }
                    else :
                        $dataReturn['status'] = true;
                        $dataReturn['message'] = sprintf('Ficheiro %s enviado para Reciclagem', $dataInput['nameFile']);
                    endif;
                }
                break;
            default:
                # code...
                $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                break;
        }
        echo json_encode($dataReturn);
    }

    public function catalogarFilesFolders()
    {
        $dataInput = $this->request->getPost(null, FILTER_SANITIZE_STRING);
        $dataInput['pathFile'] = explode('||', $dataInput['pathFile']);
        $pathDestino = fm_clean_path($dataInput['pathFile'][1] . '/' . $dataInput['fileName']);
        $idFolderDestino = $dataInput['pathFile'][0];
        $pathOrigem = fm_clean_path($dataInput['typeFile'] === 'file' ? $dataInput['origemFile'] . '/' . $dataInput['fileName'] : $dataInput['origemFile']);
        $dataReturn['status'] = false;
        $dataReturn['teste'] = [$dataInput, fm_clean_path($pathDestino . '/' . $dataInput['fileName']), $idFolderDestino, fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem];
        $fileModel = new \App\Models\FicheiroModel();
        $folderModel = new \App\Models\ArmazenamentoModel();
        $rename = fm_rename(fm_clean_path(GENERAL_PATH) . '/' . $pathOrigem, fm_clean_path(GENERAL_PATH . '/' . $pathDestino));
        if ($rename) :
            switch ($dataInput['typeFile']) {
                case 'file':
                    # code...
                    $idFicheiro = $fileModel->generate_key();
                    $fileModel->insert([
                        'idficheiro' => $idFicheiro,
                        'descricao' => $dataInput['fileName'],
                        'caminho' => $pathDestino,
                        'armazenamento' => $idFolderDestino
                    ]);
                    break;
                case 'folder':
                    $folderTarget = $folderModel->find($idFolderDestino);
                    $idFolder = $folderModel->generate_key();
                    $folderModel->insert([
                        'idarmazenamento' => $idFolder,
                        'origem' =>  $idFolderDestino,
                        'descricao' => $dataInput['fileName'],
                        'caminho' => $pathDestino,
                        'grupoAcesso' => $folderTarget->grupoAcesso
                    ]);
                    break;
                default:
                    # code...
                    $dataReturn['message'] = sprintf('Erro %s: Opcao Invalida0', __LINE__);
                    break;
            }
            if ($this->db->affectedRows() > 0) :
                $dataReturn['status'] = true;
                $dataReturn['message'] = sprintf('Ficheiro %s catalogado com sucesso', $dataInput['fileName']);
            else :
                $dataReturn['message'] = sprintf('Erro %s: Não foi possiveL salvar Alterações Solicitadas', __LINE__);
            endif;
        elseif (is_null($rename)) :
            $dataReturn['message'] = sprintf('ERROR DFF - %S: Ficheiro ou pasta com este caminha já existe', __LINE__);
        else :
            $dataReturn['message'] = sprintf('ERROR DFF-%s: Erro ao tentar Catalogar de %s ', __LINE__, $dataInput['fileName']);
        endif;

        echo json_encode($dataReturn);
    }


    public function registarGrupoAcesso()
    {
        # Registo de Grupo de Acesso de Utilizadores
        $dataReturn['status'] = false;
        if (!$this->session->isLoggedIn) :
            $dataReturn['message'] = sprintf("Erro %s: Não tem permissões para efectuar essa acção", str_pad(__LINE__, 3, "0", STR_PAD_LEFT));
        else :
            $dataInput = $this->request->getPost(null, FILTER_DEFAULT);
            $novoGrupoAcesso = new \App\Entities\Grupoacesso();
            $grupoAcesso = new \App\Models\GrupoAcessoModel();
            $idGrupoAcesso = $grupoAcesso->generatePK();
            $dataReturn['teste'] = $dataInput;
            $novoGrupoAcesso->fill([
                'idgrupoAcesso' => $idGrupoAcesso,
                'icon' => $dataInput['icone'],
                'descricao' => $dataInput['descricao'],
                'color' => $dataInput['color']
            ]);
            $grupoAcesso->insert($novoGrupoAcesso);
            if ($this->db->affectedRows() > 0) {
                $dataReturn['status'] = true;
                $dataReturn['message'] = sprintf("Registo do Grupo de Acesso %s foi registado com sucesso", $dataInput['descricao']);
            } else {
                $dataReturn['message'] = sprintf("Erro %s: Registo do Grupo de Acesso %s não foi registado com sucesso", str_pad(__LINE__, 3, "0", STR_PAD_LEFT), $dataInput['descricao']);
            }


        endif;
        echo json_encode($dataReturn);
    }

    public function pageEmissaoDocumento()
    {
        # Emissão de documentos
        if (!$this->session->isLoggedIn) :
            return redirect()->to('/utilizador/autenticacao');
        else :
            $this->dataView['currectModulo'] = 'Documentação';
            $this->dataView['currectSubModulo'] = 'Documentação';
            $this->dataView['submoduloName'] = 'Emissão de Documentos';
            $this->dataView['currectSubModulo'] = 'FCN202012613';
            $this->dataView['classPage'] = 'sidebar-mini';
            return view('pageEmissaoDocumento', $this->dataView);
        endif;
    }

    public function pagePrevDocumento(string $documentName = null)
    {
        # Previsualizar documento no formato pdf
        if (is_null($documentName)) :
            $this->dataView['content'] = $this->request->getGet('conteudo');
        else :
            $this->dataView['content'] = file_get_contents(WRITEPATH . "/rascunho/" . $documentName);
        endif;
        //return view('previsualizarConteudo', $this->dataView);
        $dompdf = new \Dompdf\Dompdf();

        $content = view('previsualizarConteudo', $this->dataView);
        $dompdf->loadHtml($content);
        $dompdf->setPaper('A4', 'portait');
        $dompdf->render();
        $dompdf->stream('preview.pdf', array("Attachment" => false));
    }

    public function modalVerPerfilUtilizador(string $idutilizador = null)
    {
        # code...
        $modelUtilizador = new \App\Models\UtilizadorModel();
        $modelGestaoAcesso = new \App\Models\GestaoAcessoModel();
        $modelFolders = new \App\Models\ArmazenamentoModel();
        $modelGrupoAcesso = new \App\Models\GrupoAcessoModel();
        $this->dataView['dadosUtilizador'] = $modelUtilizador->find($idutilizador);
        $acessosUser = $modelGestaoAcesso->where('utilizador', $idutilizador)->findAll();
        $idAcesso = array_map(function ($element) {
            return $element['grupoAcesso'];
        }, $acessosUser);
        $grupoAcesso = $modelGrupoAcesso->whereIn('idgrupoAcesso', $idAcesso)->findAll();
        $listFolders = $modelFolders->whereIn('grupoAcesso', $idAcesso)->findAll();
        $this->dataView['grupoAcesso'] = $grupoAcesso;
        $this->dataView['listFolders'] = $listFolders;
        return view('_modalVerPerfilUtilizador', $this->dataView);
    }
}
