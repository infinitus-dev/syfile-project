-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Tempo de geração: 28-Dez-2020 às 15:29
-- Versão do servidor: 10.4.11-MariaDB
-- versão do PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Banco de dados: `db_syfile_system`
--
CREATE DATABASE IF NOT EXISTS `db_syfile_system` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db_syfile_system`;

DELIMITER $$
--
-- Funções
--
DROP FUNCTION IF EXISTS `generate_custom_pk`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `generate_custom_pk` (`tipo` INT) RETURNS VARCHAR(45) CHARSET utf8 BEGIN
declare contador int;
declare new_key varchar(45);
IF tipo=1 then 
select count(*) into contador from utilizadores;
SET new_key = 'USR';
elseif tipo = 2 then
select count(*) into contador from funcionalidades;
SET new_key = 'FCN';
elseif tipo = 3 then
select count(*) into contador from armazenamentos;
SET new_key = 'FLD';
elseif tipo = 4 then
select count(*) into contador from ficheiros;
SET new_key = 'FIL';
elseif tipo = 5 then
select count(*) into contador from grupoacessos;
SET new_key = 'GAC';
else
SET new_key = 'DFL';
SET contador = round(1.0 + 9.0 * RAND());
end IF;
SET new_key = concat(new_key,year(curdate()),month(curdate()),round(10.0 + 99.0 * RAND()),contador+1);
RETURN new_key;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `armazenamentos`
--

DROP TABLE IF EXISTS `armazenamentos`;
CREATE TABLE IF NOT EXISTS `armazenamentos` (
  `idarmazenamento` varchar(45) NOT NULL,
  `origem` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `caminho` varchar(255) DEFAULT NULL,
  `grupoAcesso` varchar(45) NOT NULL,
  `color` varchar(45) DEFAULT 'info',
  `estado` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`idarmazenamento`),
  KEY `fk_armazenamentos_grupoAcessos1_idx` (`grupoAcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `armazenamentos`
--

INSERT INTO `armazenamentos` (`idarmazenamento`, `origem`, `descricao`, `caminho`, `grupoAcesso`, `color`, `estado`) VALUES
('FLD2020126312', NULL, 'Relatorios Financeiros', 'armazenamento/Relatorios Financeiros', 'GAC2020121093', 'info', 1),
('FLD2020127913', NULL, 'Relatorios Mensais', 'armazenamento/Relatorios Mensais', 'GAC202012884', 'navy', 1),
('FLD202081088', 'FLD20208404', 'Consumiveis', 'armazenamento/Finanças', 'GAC20208761', 'info', 1),
('FLD202081211', 'FLD20208276', 'teste', 'armazenamento/Informatica/teste', 'GAC20208761', 'info', 1),
('FLD202082610', 'FLD20208404', 'Consumiveis', 'armazenamento/Finanças', 'GAC20208761', 'info', 1),
('FLD20208276', NULL, 'Informatica', 'armazenamento/Informatica', 'GAC20208761', 'info', 1),
('FLD20208291', NULL, 'Temporario', 'armazenamento/temporario', 'GAC20208761', 'secondary', 1),
('FLD20208329', 'FLD20208404', 'Consumiveis', 'armazenamento/Finanças', 'GAC20208761', 'info', 1),
('FLD20208346', NULL, 'Serviços Gerais', 'armazenamento/Serviços Gerais', 'GAC20208761', 'info', 1),
('FLD20208404', NULL, 'Finanças', 'armazenamento/Finanças', 'GAC20208761', 'info', 1),
('FLD20208525', 'FLD20208925', 'Finanças', 'armazenamento/Contabilidade e Finanças/Finanças', 'GAC20208761', 'info', 1),
('FLD20208857', 'FLD20208276', 'Consumiveis', 'armazenamento/Informatica/Consumiveis', 'GAC20208761', 'info', 1),
('FLD20208925', NULL, 'Contabilidade e Finanças', 'armazenamento/Contabilidade e Finanças', 'GAC20208761', 'info', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ficheiros`
--

DROP TABLE IF EXISTS `ficheiros`;
CREATE TABLE IF NOT EXISTS `ficheiros` (
  `idficheiro` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `caminho` varchar(255) DEFAULT NULL,
  `resumo` varchar(45) DEFAULT NULL,
  `armazenamento` varchar(45) NOT NULL,
  `estado` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`idficheiro`),
  KEY `fk_ficheiros_armazenamentos1_idx` (`armazenamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `ficheiros`
--

INSERT INTO `ficheiros` (`idficheiro`, `descricao`, `caminho`, `resumo`, `armazenamento`, `estado`) VALUES
('FIL202012657', '1607085994_0a78a819ba214834a262.docx', 'armazenamento/Relatorios Financeiros/1607085994_0a78a819ba214834a262.docx', NULL, 'FLD2020126312', 1),
('FIL20208311', '1598049222_6c34f8737ac6efa5dc31.docx', 'armazenamento/Informatica/1598049222_6c34f8737ac6efa5dc31.docx', NULL, 'FLD20208276', 0),
('FIL20208826', 'more thigns.txt', 'armazenamento/Finanças/more thigns.txt', NULL, 'FLD202081088', 1),
('FIL20208864', 'teste.png', 'armazenamento/Contabilidade e Finanças/Finanças/teste.png', NULL, 'FLD20208525', 1),
('FIL20208872', '1598049222_2904cc64c158d46865a3.pdf', 'armazenamento/Contabilidade e Finanças/1598049222_2904cc64c158d46865a3.pdf', NULL, 'FLD20208925', 1),
('FIL20208935', 'teste.png', 'armazenamento/Serviços Gerais/teste.png', NULL, 'FLD20208346', 1),
('FIL20208996', '1598049222_6c34f8737ac6efa5dc31.docx', 'armazenamento/Finanças/1598049222_6c34f8737ac6efa5dc31.docx', NULL, 'FLD20208404', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `funcionalidades`
--

DROP TABLE IF EXISTS `funcionalidades`;
CREATE TABLE IF NOT EXISTS `funcionalidades` (
  `idfuncionalidade` varchar(45) NOT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `modulo` varchar(45) DEFAULT NULL,
  `link` varchar(45) DEFAULT NULL,
  `icon` varchar(45) DEFAULT 'fas fa-th',
  `nivelAcesso` int(11) DEFAULT 0,
  `estado` tinyint(4) DEFAULT 1,
  PRIMARY KEY (`idfuncionalidade`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `funcionalidades`
--

INSERT INTO `funcionalidades` (`idfuncionalidade`, `descricao`, `modulo`, `link`, `icon`, `nivelAcesso`, `estado`) VALUES
('FCN202012613', 'Emissão Documento', 'Documentação', 'documentacao/emissao-documento', 'fas fa-clipboard', 2, 1),
('FCN20208462', 'Utilizadores', 'Gestão de Utilizadores', 'utilizadores/listar-utilizadores', 'fas fa-th', 0, 1),
('FCN20208641', 'Registo Utilizador', 'Gestão de Utilizadores', 'utilizadores/novo-registo', 'fas fa-th', 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `gestaoacessos`
--

DROP TABLE IF EXISTS `gestaoacessos`;
CREATE TABLE IF NOT EXISTS `gestaoacessos` (
  `utilizador` varchar(45) NOT NULL,
  `grupoAcesso` varchar(45) NOT NULL,
  `visualizar` enum('sim','não') DEFAULT 'sim',
  `transferir` enum('sim','não') DEFAULT 'não',
  `mover` enum('sim','não') DEFAULT 'não',
  `copiar` enum('sim','não') DEFAULT 'não',
  `eliminar` enum('sim','não') DEFAULT 'não',
  `editar` enum('sim','não') DEFAULT 'não',
  KEY `fk_gestaoAcessos_utilizadores1_idx` (`utilizador`),
  KEY `fk_gestaoAcessos_grupoAcessos1_idx` (`grupoAcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `gestaoacessos`
--

INSERT INTO `gestaoacessos` (`utilizador`, `grupoAcesso`, `visualizar`, `transferir`, `mover`, `copiar`, `eliminar`, `editar`) VALUES
('USR20208616', 'GAC20208761', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim'),
('USR20208357', 'GAC20208761', 'sim', 'não', 'sim', 'sim', 'sim', 'não'),
('USR20208463', 'GAC20208761', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim'),
('USR202012804', 'GAC2020121093', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim'),
('USR202012295', 'GAC2020121093', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim'),
('USR202012295', 'GAC20208761', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim'),
('USR20208616', 'GAC202012884', 'sim', 'sim', 'sim', 'sim', 'sim', 'sim');

-- --------------------------------------------------------

--
-- Estrutura da tabela `grupoacessos`
--

DROP TABLE IF EXISTS `grupoacessos`;
CREATE TABLE IF NOT EXISTS `grupoacessos` (
  `idgrupoAcesso` varchar(45) NOT NULL,
  `icon` varchar(45) DEFAULT NULL,
  `descricao` varchar(45) DEFAULT NULL,
  `color` varchar(45) DEFAULT 'warning',
  PRIMARY KEY (`idgrupoAcesso`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `grupoacessos`
--

INSERT INTO `grupoacessos` (`idgrupoAcesso`, `icon`, `descricao`, `color`) VALUES
('GAC2020121093', 'fas fa-hand-holding-usd', 'Financeiro', 'warning'),
('GAC202012884', 'fas fa-donate', 'Contabilidade', 'warning'),
('GAC202012893', 'fas fa-tools', 'Tecnologias de Informação', 'purple'),
('GAC20208761', 'fa fa-users', 'Geral', 'warning');

--
-- Acionadores `grupoacessos`
--
DROP TRIGGER IF EXISTS `grupoacessos_AFTER_INSERT`;
DELIMITER $$
CREATE TRIGGER `grupoacessos_AFTER_INSERT` AFTER INSERT ON `grupoacessos` FOR EACH ROW BEGIN
insert into gestaoacessos (`utilizador`, `grupoAcesso`, `visualizar`, `transferir`, `mover`, `copiar`, `eliminar`, `editar`) values ('USR20208616', new.idgrupoAcesso, 'sim', 'sim', 'sim', 'sim', 'sim', 'sim');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `icones`
--

DROP TABLE IF EXISTS `icones`;
CREATE TABLE IF NOT EXISTS `icones` (
  `idicone` int(11) NOT NULL AUTO_INCREMENT,
  `class` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idicone`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `icones`
--

INSERT INTO `icones` (`idicone`, `class`) VALUES
(1, 'fas fa-hand-holding-usd'),
(2, 'fas fa-file-invoice-dollar'),
(3, 'fas fa-stamp'),
(4, 'fas fa-coins'),
(5, 'fas fa-comments-dollar'),
(6, 'fas fa-chart-pie'),
(7, 'fas fa-balance-scale-left'),
(8, 'fas fa-cash-register'),
(9, 'fas fa-donate'),
(10, 'fas fa-user-friends'),
(11, 'fas fa-user-shield'),
(12, 'fas fa-users'),
(13, 'fas fa-user-lock'),
(14, 'fas fa-users-cog'),
(15, 'fas fa-user-tag'),
(16, 'fas fa-user-graduate'),
(17, 'fas fa-user-tie'),
(18, 'fas fa-user-nurse'),
(19, 'fas fa-user-md'),
(20, 'fas fa-street-view'),
(21, 'fas fa-truck-loading'),
(22, 'fas fa-archive'),
(23, 'fas fa-pallet'),
(24, 'fas fa-toolbox'),
(25, 'fas fa-laptop-house'),
(26, 'fas fa-tools'),
(27, 'fas fa-wrench');

-- --------------------------------------------------------

--
-- Estrutura da tabela `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `version` varchar(255) NOT NULL,
  `class` text NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `migrations`
--

INSERT INTO `migrations` (`id`, `version`, `class`, `group`, `namespace`, `time`, `batch`) VALUES
(1, '2020-01-19-200313', 'Auth\\Database\\Migrations\\CreateUsersTable', 'default', 'App', 1597694312, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `perfilutilizadores`
--

DROP TABLE IF EXISTS `perfilutilizadores`;
CREATE TABLE IF NOT EXISTS `perfilutilizadores` (
  `perfil` varchar(45) NOT NULL,
  `nivelAcesso` int(11) DEFAULT 1,
  PRIMARY KEY (`perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `perfilutilizadores`
--

INSERT INTO `perfilutilizadores` (`perfil`, `nivelAcesso`) VALUES
('administrador', 0),
('utlizador', 2);

-- --------------------------------------------------------

--
-- Estrutura da tabela `permissoes`
--

DROP TABLE IF EXISTS `permissoes`;
CREATE TABLE IF NOT EXISTS `permissoes` (
  `descricao` varchar(45) NOT NULL,
  `atributoBd` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`descricao`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `permissoes`
--

INSERT INTO `permissoes` (`descricao`, `atributoBd`) VALUES
('Copiar', 'copiar'),
('Editar', 'editar'),
('Eliminar', 'eliminar'),
('Mover', 'mover'),
('Transferir', 'transferir'),
('Visualizar', 'visualizar');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `email` varchar(191) NOT NULL,
  `new_email` varchar(191) DEFAULT NULL,
  `password_hash` varchar(191) NOT NULL,
  `name` varchar(191) NOT NULL,
  `activate_hash` varchar(191) DEFAULT NULL,
  `reset_hash` varchar(191) DEFAULT NULL,
  `reset_expires` bigint(20) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` bigint(20) DEFAULT NULL,
  `updated_at` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estrutura da tabela `utilizadores`
--

DROP TABLE IF EXISTS `utilizadores`;
CREATE TABLE IF NOT EXISTS `utilizadores` (
  `idutilizador` varchar(45) NOT NULL,
  `nome` varchar(45) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `estado` tinyint(4) DEFAULT 1,
  `updated_at` datetime DEFAULT NULL,
  `avatar` varchar(45) DEFAULT 'default.png',
  `perfil` enum('administrador','utlizador') DEFAULT 'utlizador',
  PRIMARY KEY (`idutilizador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `utilizadores`
--

INSERT INTO `utilizadores` (`idutilizador`, `nome`, `password`, `created_at`, `email`, `estado`, `updated_at`, `avatar`, `perfil`) VALUES
('USR202012295', 'Sergio Jose', '$2y$10$bWIzrYZArkcNa5E/7FcE5e.FK5p6VJ7O7BOGgtgwgYSP2nUlGqL0q', '2020-12-04 14:45:22', 'sergio@jose.com', 1, '2020-12-04 14:45:22', 'default.png', 'utlizador'),
('USR202012804', 'Mario Manuel', '$2y$10$LfXFzWdzVNkjG7iiUjsYSuV.vu5GTvZWZoOmUhtkAVRbeofC1Xvt.', '2020-12-04 12:26:39', 'mario.manuel@live.com.pt', 1, '2020-12-04 12:26:39', 'default.png', 'utlizador'),
('USR20208357', 'Julio Fernandes', '$2y$10$W6Jvr/tt8S3fYV4mf.3kButwt2LBgsS4vFhH5kvpdC6rZUCITCKzq', '2020-08-16 14:53:40', 'julio@fernandes.com', 1, '2020-08-16 14:53:40', 'default.png', 'utlizador'),
('USR20208463', 'Joao Sebastiao', '$2y$10$5ig2qFp17UqvU7JMacCUVuWSOw32PPvCV0WLYUcpWxJgIIqBP7fFG', '2020-08-21 22:09:55', 'joao@sebastiao', 1, '2020-08-21 22:09:55', 'default.png', 'utlizador'),
('USR20208616', 'Administrador', '$2y$10$mPRyQmWj.ajWpVAISst8Qer3u5bYc9BQnciT/DbFMyYVOJxKk9ZpC', '2020-08-16 14:53:03', 'admin@syfile.com', 1, '2020-08-16 14:53:03', 'default.png', 'administrador');

--
-- Restrições para despejos de tabelas
--

--
-- Limitadores para a tabela `armazenamentos`
--
ALTER TABLE `armazenamentos`
  ADD CONSTRAINT `fk_armazenamentos_grupoAcessos1` FOREIGN KEY (`grupoAcesso`) REFERENCES `grupoacessos` (`idgrupoAcesso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `ficheiros`
--
ALTER TABLE `ficheiros`
  ADD CONSTRAINT `fk_ficheiros_armazenamentos1` FOREIGN KEY (`armazenamento`) REFERENCES `armazenamentos` (`idarmazenamento`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Limitadores para a tabela `gestaoacessos`
--
ALTER TABLE `gestaoacessos`
  ADD CONSTRAINT `fk_gestaoAcessos_grupoAcessos1` FOREIGN KEY (`grupoAcesso`) REFERENCES `grupoacessos` (`idgrupoAcesso`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_gestaoAcessos_utilizadores1` FOREIGN KEY (`utilizador`) REFERENCES `utilizadores` (`idutilizador`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
