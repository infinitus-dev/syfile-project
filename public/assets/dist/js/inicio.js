function registoAjax(e, form, titulo) {
    var formData = new FormData(form[0]);
    $.ajax({
        url: e.delegateTarget.action,
        type: 'post',
        dataType: 'json',
        data: formData,
        contentType: false,
        processData: false,
        success: (dataReturn) => {
            var messageReturn = { icon: null, message: dataReturn.message };
            if (dataReturn.status) {
                messageReturn.icon = 'success';
            } else {
                messageReturn.icon = 'warning';
            }
            Swal.fire({
                title: titulo,
                text: messageReturn.message,
                icon: messageReturn.icon,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fechar',
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        },
        error: (dataReturn, xhr) => {
            console.log(dataReturn);
            console.log(xhr);
            Swal.fire({
                title: titulo,
                text: "Error: " + xhr + " - Não foi possivel efectuar o registo. Contactar Administrador de Sistema!",
                icon: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fechar!',
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        }
    })
}

servicoModalAjax = (url, targetID, callback, titulo, data = {}) => {
    $.ajax({
        type: "get",
        url: url,
        data: data,
        dataType: "html",
        success: (response) => {
            $(targetID).find('.modal-body').html(response);
            $(targetID).find('.modal-header').find('.modal-title').html(titulo);
            $(targetID).modal('show');
            callback();
        },
        error: (dataReturn, xhr) => {
            console.log(dataReturn);
            console.log(xhr);
            Swal.fire({
                title: titulo,
                text: "Error: " + xhr + " - Não foi possivel processar a solicitação. Contactar Administrador de Sistema!",
                icon: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fechar!',
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        }
    });
};

$(() => {
    "use strict";

    $('.linesIcones').each((index, element) => {
        $(element).hover(() => {
            $(element).find('.lineicon').addClass('fa-folder-open');
            $(element).find('.lineicon').removeClass('fa-folder');
        }, () => {
            $(element).find('.lineicon').addClass('fa-folder');
            $(element).find('.lineicon').removeClass('fa-folder-open');
        })
    });

    var formCriarPasta = $("#modal-criarFolder form");
    var formUploadFicheiro = $("#modal-uploadArchive form");
    formCriarPasta.submit((e) => {
        e.preventDefault();
        registoAjax(e, formCriarPasta, "Registo de Pasta");
    });

    formUploadFicheiro.submit((e) => {
        e.preventDefault();
        registoAjax(e, formUploadFicheiro, "Carregamento de Ficheiros");
    });
})