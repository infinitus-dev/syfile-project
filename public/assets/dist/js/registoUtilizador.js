$(() => {
    "use strict";
    let formRegistarGrupoAcesso = $("#formRegistarGrupoAcesso");
    /*
        Code Number: 10001
        Descrição:Verificação de Grupos Activos
        Função: Desativar as permissões dos grupos desactivados
     */
    $(".linesAcessos").each((index, element) => {
        console.log(index);
        $(element).find('.grupoAcesso input').each((indexAcesso, elementAcesso) => {
            $(elementAcesso).change((e) => {
                console.log(e.delegateTarget.checked);
                $(element).find('.linesPermissoes input').each((indexPermissao, elementPermissao) => {
                    $(elementPermissao).attr('disabled', !e.delegateTarget.checked);
                });

            })
        });
    });
    /*
    END 10001
     */

    /*
        Code Number: 10002
        Descrição: Registo de Novo Utilizador
        Função: Registar um novo utilizador para acesso ao sistema
     */
    $("#formRegistarUtilizador").submit((e) => {
        e.preventDefault();
        var form = $("#formRegistarUtilizador");
        $.ajax({
            url: e.delegateTarget.action,
            data: form.serialize(),
            dataType: 'json',
            type: 'post',
            success: (dataReturn) => {
                console.log(dataReturn);
                var message = { icon: null, content: null };
                if (dataReturn.status) {
                    message.icon = 'success';
                    message.content = 'Utilizador foi registado com sucesso';
                } else {
                    message.icon = 'warning';
                    message.content = dataReturn.message
                }
                Swal.fire({
                    title: 'Registo de Utilizador',
                    text: message.content,
                    icon: message.icon,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Fechar!',
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            },
            error: (data, xhr) => {
                Swal.fire({
                    title: 'Registo de Utilizador',
                    text: "Error: " + xhr + " - Não foi possivel efectuar o registo. Contactar Administrador de Sistema!",
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Fechar!',
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                });
                console.log(data);
                console.log(xhr);
            }
        })
    });
    formRegistarGrupoAcesso.submit((e) => {
        e.preventDefault();
        $.ajax({
            url: e.delegateTarget.action,
            data: formRegistarGrupoAcesso.serialize(),
            dataType: 'json',
            type: 'post',
            success: (dataReturn) => {
                console.log(dataReturn);
                var message = { icon: null, content: null };
                if (dataReturn.status) {
                    message.icon = 'success';
                } else {
                    message.icon = 'warning';
                }
                message.content = dataReturn.message;
                Swal.fire({
                    title: 'Registo de Grupo de Acesso',
                    text: message.content,
                    icon: message.icon,
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Fechar!',
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                })
            },
            error: (data, xhr) => {
                Swal.fire({
                    title: 'Registo de Grupo de Acesso',
                    text: "Error: " + xhr + " - Não foi possivel efectuar o registo. Contactar Administrador de Sistema!",
                    icon: 'warning',
                    showCancelButton: false,
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Fechar!',
                    allowOutsideClick: false,
                }).then((result) => {
                    if (result.value) {
                        location.reload();
                    }
                });
                console.log(data);
                console.log(xhr);
            }
        })
    });

});