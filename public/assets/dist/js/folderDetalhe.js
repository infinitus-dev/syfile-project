/*
    Code Number: 10000
    Descrição: Submit Formulario Via AJAX
    Função: Submeter dados vindo dos formulario usando conexão AJAX e retornar as mensagens do submit vindas do Back-end
*/
ajaxSubmit = (e, form, titulo) => {
    $.ajax({
        url: e.delegateTarget.action,
        data: form.serialize(),
        dataType: 'json',
        type: 'post',
        success: (dataReturn) => {
            console.log(dataReturn);
            var messageReturn = { icon: null, message: dataReturn.message };
            if (dataReturn.status) {
                messageReturn.icon = 'success';
            } else {
                messageReturn.icon = 'warning';
            }
            Swal.fire({
                title: titulo,
                text: messageReturn.message,
                icon: messageReturn.icon,
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fechar',
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        },
        error: (dataReturn, xhr) => {
            console.log(dataReturn);
            console.log(xhr);
            Swal.fire({
                title: titulo,
                text: "Error: " + xhr + " - Não foi possivel efectuar o registo. Contactar Administrador de Sistema!",
                icon: 'warning',
                showCancelButton: false,
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Fechar!',
                allowOutsideClick: false,
            }).then((result) => {
                if (result.value) {
                    location.reload();
                }
            });
        }
    });
};


/*
    Code Number: 10001
    Descrição: Abrir modal de Catalogação de Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de catalogaçao de ficheiros nao registado na BD
*/
catalogacao = (pathItem, nomeItem, tipoItem, icon = 'fas fa-folder') => {
    var modalCatalogarItens = $("#modalCatalogarItens");
    modalCatalogarItens.modal('show');

    modalCatalogarItens.on('shown.bs.modal', () => {
        var formCatalogarFiles = modalCatalogarItens.find("form#formCatalogarFiles");
        formCatalogarFiles.find("#iconContent").html('<i class="fa-5x ' + icon + '"></i>');
        formCatalogarFiles.find("#fileNameCtl").val(nomeItem);
        formCatalogarFiles.find("#oldName").val(nomeItem);
        formCatalogarFiles.find("#origemFile").val(pathItem);
        formCatalogarFiles.find("#typeFile").val(tipoItem);
        formCatalogarFiles.submit((e) => {
            e.preventDefault();
            ajaxSubmit(e, formCatalogarFiles, 'Catalogação de Ficheiros e Pastas');
        });

    });
};

/*
    Code Number: 10002
    Descrição: Abrir modal de Edição de Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de edição de nome de ficheiro e pasta e submeter alteraçoes na BF
*/
edicaoFile = (pathItem, nomeItem, icon, idItem) => {
    var modalEdicaoFile = $("#modalEditarNome");
    modalEdicaoFile.modal('show');
    modalEdicaoFile.on("shown.bs.modal", () => {
        var formEdicaoNomeFile = modalEdicaoFile.find("form#formEditarFile");
        formEdicaoNomeFile.find("#editContentIcon").html('<i class="fa-5x ' + icon + '"></i>');
        formEdicaoNomeFile.find("#nomeFicheiroEditar").val(nomeItem);
        formEdicaoNomeFile.find("#idItemEdit").val(idItem);
        formEdicaoNomeFile.find("#oldNameFileEdit").val(nomeItem);
        formEdicaoNomeFile.find("#pathFileEdit").val(pathItem);
        formEdicaoNomeFile.submit((e) => {
            e.preventDefault();
            registoAjax(e, formEdicaoNomeFile, 'Edição Nome Ficheiro');
        });

    });
}

/*
    Code Number: 10003
    Descrição: Abrir modal de Copiar e Movimentar Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de edição de nome de ficheiro e pasta e submeter alteraçoes na BF
*/

copiarMoverFiles = (pathItem, nomeItem, icon = 'fas fa-folder', idItem, type = 'folder') => {
    modalCopiarMoverFiles = $("#modalCopiarMoverFiles");

    modalCopiarMoverFiles.modal('show');
    modalCopiarMoverFiles.on('shown.bs.modal', (event) => {
        formCopiarMoverFiles = modalCopiarMoverFiles.find("form#formCopiarMoverFiles");
        modalCopiarMoverFiles.find("#iconMoveCopy").html('<i class="' + icon + ' fa-2x"></i>');
        modalCopiarMoverFiles.find("#contentFileMoveCopy").html(pathItem);
        modalCopiarMoverFiles.find("#contentNameFileMoveCopy").html(nomeItem);
        formCopiarMoverFiles.find("#pathMoveCopy").val(pathItem);
        formCopiarMoverFiles.find("#nameFileMoveCopy").val(nomeItem);
        formCopiarMoverFiles.find("#idItemFileMoveCopy").val(idItem);
        formCopiarMoverFiles.find("#typeFileMoveCopy").val(type);
        console.log([pathItem, nomeItem, icon, idItem, type]);
        formCopiarMoverFiles.submit((e) => {
            e.preventDefault();
            registoAjax(e, formCopiarMoverFiles, 'Mover/Copiar Ficheiro');
        })
    });

}

/*
    Code Number: 10005
    Descrição: Mandar Pasta ou Ficheiro Para o Lixo
    Função: Confirmar decisao do User de Eliminar (Mover para o lixo) permanentemento para o ficheiro ou pasta
*/

eliminarFilesFoldes = (pathItem, nomeItem, idItem, icon, type) => {
    modalEliminarFilesFolders = $("#modalEliminarFilesFolders");
    console.log([pathItem, nomeItem, idItem]);
    modalEliminarFilesFolders.modal('show');
    modalEliminarFilesFolders.on('shown.bs.modal', () => {
        formEliminarFilesFolders = modalEliminarFilesFolders.find("form#formEliminarFilesFolders");
        modalEliminarFilesFolders.find("#iconDeleteFile").html('<i class="fa-2x ' + icon + '"></i>');
        modalEliminarFilesFolders.find("#contentNameFileDelete").html(nomeItem);
        formEliminarFilesFolders.find("#idItemDelete").val(idItem);
        formEliminarFilesFolders.find("#nameItemDelete").val(nomeItem);
        formEliminarFilesFolders.find("#pathItemDelete").val(pathItem);
        formEliminarFilesFolders.find("#typeItemDelete").val(type);
        formEliminarFilesFolders.submit((e) => {
            e.preventDefault();
            console.log(formEliminarFilesFolders.serializeArray());
            registoAjax(e, formEliminarFilesFolders, 'Eliminar Ficheiros/Pastas');
        });
    });
    /*
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Sim, Remover!'
    }).then((result) => {
        if (result.value) {
            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })*/
}


$(() => {
    "use strict";

    /*
        Code Number: 10004
        Descrição: Habilita a ediçao do nome de ficheiro ou pasta a ser catalogado
        Função: Remover ou adicionar o attr readonly
     */
    $("#modalCatalogarItens form#formCatalogarFiles #btnEditName").click((e) => {
        var elemnt = $("#modalCatalogarItens form#formCatalogarFiles #fileNameCtl");
        if (typeof elemnt.attr('readonly') !== typeof undefined && elemnt.attr('readonly') !== false) {
            elemnt.removeAttr('readonly');
        } else {
            elemnt.attr('readonly', true);
        }
    });

    $("input[data-bootstrap-switch]").each(function () {
        $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

    var formCriarFolderLocal = $("#formCriarFolderLocal");
    var formLocalUploadArchive = $("#formLocalUploadArchive");
    formCriarFolderLocal.submit((e) => {
        e.preventDefault();
        registoAjax(e, formCriarFolderLocal, "Registo de Pasta");
    });

    formLocalUploadArchive.submit((e) => {
        e.preventDefault();
        registoAjax(e, formLocalUploadArchive, "Carregamento de Ficheiros");
    });

})