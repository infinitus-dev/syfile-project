"use strict";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

/*
    Code Number: 10000
    Descrição: Submit Formulario Via AJAX
    Função: Submeter dados vindo dos formulario usando conexão AJAX e retornar as mensagens do submit vindas do Back-end
*/
ajaxSubmit = function ajaxSubmit(e, form, titulo) {
  $.ajax({
    url: e.delegateTarget.action,
    data: form.serialize(),
    dataType: 'json',
    type: 'post',
    success: function success(dataReturn) {
      console.log(dataReturn);
      var messageReturn = {
        icon: null,
        message: dataReturn.message
      };

      if (dataReturn.status) {
        messageReturn.icon = 'success';
      } else {
        messageReturn.icon = 'warning';
      }

      Swal.fire({
        title: titulo,
        text: messageReturn.message,
        icon: messageReturn.icon,
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Fechar',
        allowOutsideClick: false
      }).then(function (result) {
        if (result.value) {
          location.reload();
        }
      });
    },
    error: function error(dataReturn, xhr) {
      console.log(dataReturn);
      console.log(xhr);
      Swal.fire({
        title: titulo,
        text: "Error: " + xhr + " - Não foi possivel efectuar o registo. Contactar Administrador de Sistema!",
        icon: 'warning',
        showCancelButton: false,
        confirmButtonColor: '#3085d6',
        confirmButtonText: 'Fechar!',
        allowOutsideClick: false
      }).then(function (result) {
        if (result.value) {
          location.reload();
        }
      });
    }
  });
};
/*
    Code Number: 10001
    Descrição: Abrir modal de Catalogação de Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de catalogaçao de ficheiros nao registado na BD
*/


catalogacao = function catalogacao(pathItem, nomeItem, tipoItem) {
  var icon = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 'fas fa-folder';
  var modalCatalogarItens = $("#modalCatalogarItens");
  modalCatalogarItens.modal('show');
  modalCatalogarItens.on('shown.bs.modal', function () {
    var formCatalogarFiles = modalCatalogarItens.find("form#formCatalogarFiles");
    formCatalogarFiles.find("#iconContent").html('<i class="fa-5x ' + icon + '"></i>');
    formCatalogarFiles.find("#fileNameCtl").val(nomeItem);
    formCatalogarFiles.find("#oldName").val(nomeItem);
    formCatalogarFiles.find("#origemFile").val(pathItem);
    formCatalogarFiles.find("#typeFile").val(tipoItem);
  });
};
/*
    Code Number: 10002
    Descrição: Abrir modal de Edição de Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de edição de nome de ficheiro e pasta e submeter alteraçoes na BF
*/


edicaoFile = function edicaoFile(pathItem, nomeItem, icon, idItem) {
  var modalEdicaoFile = $("#modalEditarNome");
  modalEdicaoFile.modal('show');
  modalEdicaoFile.on("shown.bs.modal", function () {
    var formEdicaoNomeFile = modalEdicaoFile.find("form#formEditarFile");
    formEdicaoNomeFile.find("#editContentIcon").html('<i class="fa-5x ' + icon + '"></i>');
    formEdicaoNomeFile.find("#nomeFicheiroEditar").val(nomeItem);
    formEdicaoNomeFile.find("#idItemEdit").val(idItem);
    formEdicaoNomeFile.find("#oldNameFileEdit").val(nomeItem);
    formEdicaoNomeFile.find("#pathFileEdit").val(pathItem);
    formEdicaoNomeFile.submit(function (e) {
      e.preventDefault();
      registoAjax(e, formEdicaoNomeFile, 'Edição Nome Ficheiro');
    });
  });
};
/*
    Code Number: 10003
    Descrição: Abrir modal de Copiar e Movimentar Ficheiros e Pastas
    Função: Abre modal de modo a visualizar o formulario de edição de nome de ficheiro e pasta e submeter alteraçoes na BF
*/


copiarMoverFiles = function copiarMoverFiles(pathItem, nomeItem) {
  var icon = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 'fas fa-folder';
  var idItem = arguments.length > 3 ? arguments[3] : undefined;
  var type = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 'folder';
  modalCopiarMoverFiles = $("#modalCopiarMoverFiles");
  modalCopiarMoverFiles.modal('show');
  modalCopiarMoverFiles.on('shown.bs.modal', function (event) {
    formCopiarMoverFiles = modalCopiarMoverFiles.find("form#formCopiarMoverFiles");
    modalCopiarMoverFiles.find("#iconMoveCopy").html('<i class="' + icon + ' fa-2x"></i>');
    modalCopiarMoverFiles.find("#contentFileMoveCopy").html(pathItem);
    modalCopiarMoverFiles.find("#contentNameFileMoveCopy").html(nomeItem);
    formCopiarMoverFiles.find("#pathMoveCopy").val(pathItem);
    formCopiarMoverFiles.find("#nameFileMoveCopy").val(nomeItem);
    formCopiarMoverFiles.find("#idItemFileMoveCopy").val(idItem);
    formCopiarMoverFiles.find("#typeFileMoveCopy").val(type);
    console.log([pathItem, nomeItem, icon, idItem, type]);
    formCopiarMoverFiles.submit(function (e) {
      e.preventDefault();
      registoAjax(e, formCopiarMoverFiles, 'Mover/Copiar Ficheiro');
    });
  });
};
/*
    Code Number: 10005
    Descrição: Mandar Pasta ou Ficheiro Para o Lixo
    Função: Confirmar decisao do User de Eliminar (Mover para o lixo) permanentemento para o ficheiro ou pasta
*/


eliminarFilesFoldes = function eliminarFilesFoldes(pathItem, nomeItem, idItem) {
  console.log([pathItem, nomeItem, idItem]);
  Swal.fire({
    title: 'Are you sure?',
    text: "You won't be able to revert this!",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Sim, Remover!'
  }).then(function (result) {
    if (result.value) {
      Swal.fire('Deleted!', 'Your file has been deleted.', 'success');
    }
  });
};

$(function () {
  "use strict";
  /*
      Code Number: 10004
      Descrição: Habilita a ediçao do nome de ficheiro ou pasta a ser catalogado
      Função: Remover ou adicionar o attr readonly
   */

  $("#modalCatalogarItens form#formCatalogarFiles #btnEditName").click(function (e) {
    var elemnt = $("#modalCatalogarItens form#formCatalogarFiles #fileNameCtl");

    if (_typeof(elemnt.attr('readonly')) !== (typeof undefined === "undefined" ? "undefined" : _typeof(undefined)) && elemnt.attr('readonly') !== false) {
      elemnt.removeAttr('readonly');
    } else {
      elemnt.attr('readonly', true);
    }
  });
  $("input[data-bootstrap-switch]").each(function () {
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  });
});